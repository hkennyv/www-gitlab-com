- name: Estimated Total Monthly Active Users (TMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The estimated sum of all stage monthly active users (<a href="https://about.gitlab.com/handbook/product/performance-indicators/#stage-monthly-active-users-smau">SMAU</a>)
    in a rolling 28 day period for Self-Managed and SaaS. We extrapolate estimated TMAU based on the percentage of self-managed instances that report this data (not all self-managed instances do).  Therefore the recorded number is not the same as the estimated or even the actual number. The chart currently reflects recorded TMAU. We plan to change this to estimated TMAU [soon](https://gitlab.com/gitlab-data/analytics/-/issues/6000). Numbers displayed below are for self-managed instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>), 
    including the self-managed instance hosting the SaaS product. 
    Data for (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9128022&udv=0">TMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import. Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 10041080
    dashboard: 751556
    embed: v2

- name: Paid Total Monthly Active Users (Paid TMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all (<a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-stage-monthly-active-users-paid-smau">paid stage monthly active users </a>) in a 28 day rolling period. The license information used to generate this metric from usage ping is being validated, and the currently displayed data could be incorrect by up to 20%. Numbers displayed below are for self-managed instances with usage ping(<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>). Data for (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9279567&udv=964756">Paid TMAU for SaaS </a>) is available separately using GitLab.com Postgres Database Import. Owner of this KPI is the VP, Product Management. 
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 10040563
    dashboard: 751556
    embed: v2
  sisense_data_secondary:
    chart: 9984566
    dashboard: 751556
    embed: v2

- name: Unique Monthly Active Users (UMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users that performed an <a href="https://docs.gitlab.com/ee/api/events.html">event</a> within the previous 28 days for Self-Managed and SaaS. Numbers displayed below are for self-managed instances with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>) and GitLab.com. Data for (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=9542820&udv=0"> Paid Self-Managed (EE) Unique Monthly Active Users (UMAU) (28-day trailing window) </a>). Data for (<a href="https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8079819&udv=0"> SaaS GitLab.com Unique Monthly Active Users (UMAU) (28-day trailing window) </a>) is also available using GitLab.com Postgres Database Import. UMAU calculation is the same using both data sources (usage ping and GitLab.com Postgres Database Import). Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 9968500
    dashboard: 751556
    embed: v2

- name: New Group Namespace Trial to Paid Conversion Rate 
  base_path: "/handbook/product/performance-indicators/"
  definition: The conversion rate of a new group namespace on a trial to a <a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-namespace">paid namespace</a>. This is calculated by dividing the number of eligible new group namespaces that were on a trial and converted to paid within 90 days of creation by the total number of eligible new group namespaces that were on a trial within 90 days of creation. Eligible namespaces exclude personal namespaces, sub-group namespaces, and group namespaces that belong to a user that is on a paid plan. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: SaaS is TBD (FY21 Q4 focus)
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)


- name: New Group Namespace Create Stage Adoption Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: The adoption rate of a new group namespace adopting features on  <a href="https://about.gitlab.com/handbook/product/product-categories/#create-stage">the Create stage </a>. This is calculated as the % of eligible new group namespaces the have at least one Create SMAU action within the first 90 days of creation. Eligible namespaces exclude personal namespaces, sub-group namespaces, and group namespaces that belong to a user that is on a paid plan. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: SaaS is TBD (FY21 Q4 focus).
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)



- name: New Group Namespace Verify Stage Adoption Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: The adoption rate of a new group namespace adopting features on  <a href="https://about.gitlab.com/handbook/product/product-categories/#verify-stage">the Verify stage </a>. This is calculated as the % of eligible new group namespaces the have at least one Verify SMAU action within the first 90 days of creation. Eligible namespaces exclude personal namespaces, sub-group namespaces, and group namespaces that belong to a user that is on a paid plan. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth.
  target: SaaS is TBD (FY21 Q4 focus). 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)

- name: New Group Namespace with at least two users added 
  base_path: "/handbook/product/performance-indicators/"
  definition: The percentage of a new group namespace with at least two users added within 90 days of namespace creation. This is calculated as the % of eligible new group namespace with at least two users added within 90 days of namespace creation. Eligible namespaces exclude personal namespaces, sub-group namespaces, and group namespaces that belong to a user that is on a paid plan. The data source is GitLab.com Postgres Database Import. The owner of this KPI is the Director of PM, Growth. 
  target: SaaS is TBD (FY21 Q4 focus). 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
    - https://app.periscopedata.com/app/gitlab/761347/New-Growth-KPIs-(WIP)
 

- name: Stages per User (SpU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stages per user is calculated by dividing <a href="https://about.gitlab.com/handbook/product/performance-indicators/#estimated-total-monthly-active-users-tmau">Stage
    Monthly Active User (TMAU)</a>  by <a href="https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau">Monthly
    Active Users (UMAU)</a>. The Stages per User (SpU) KPI is meant to capture the
    number of DevOps stages the average user is using on a monthly basis. This metric
    is important, as each stage added <a href="https://about.gitlab.com/direction/#strategic-response">triples
    paid conversion</a>.  Owner of this KPI is the VP, Product Management.
  target: Our Target SpU is 1.8.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 8490496
    dashboard: 527913
    embed: v2
- name: Category Maturity Achievement
  base_path: "/handbook/product/performance-indicators/"
  definition: Percentage of category maturity plan achieved per quarter. Owner of
    this KPI is the VP, Product Management.
  target: Our Target is 70%.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 9590555
    dashboard: 734231
    embed: v2  

- name: Paid Net Promoter Score (PNPS)
  base_path: "/handbook/product/performance-indicators/"
  definition: Abbreviated as the PNPS acronym, please do not refer to it as NPS to
    prevent confusion. Measured as the percentage of paid customer "promoters" minus
    the percentage of paid customer "detractors" from a <a href="https://en.wikipedia.org/wiki/Net_Promoter#targetText=Net%20Promoter%20or%20Net%20Promoter,be%20correlated%20with%20revenue%20growth">Net
    Promoter Score</a> survey.  Note that while other teams at GitLab use a <a href="https://about.gitlab.com/handbook/business-ops/data-team/metrics/#satisfaction">satisfaction
    score</a>, we have chosen to use PNPS in this case so it is easier to benchmark
    versus other like companies.  Also note that the score will likely reflect customer
    satisfaction beyond the product itself, as customers will grade us on the total
    customer experience, including support, documentation, billing, etc. Owner of
    this KPI is Product Operations.
  target: Our Target PNPS is 40.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 8521210
    dashboard: 527913
    embed: v2
  sisense_data_secondary:
    chart: 9184725
    dashboard: 527913
    embed: v2

- name: Active Hosts
  base_path: "/handbook/product/performance-indicators/"
  definition: The count of active <a href="https://about.gitlab.com/pricing/#self-managed">Self
    Hosts</a>, Core and Paid, plus GitLab.com. Owner of this KPI is VP, Product Management.
    This is measured by counting the number of unique GitLab instances that send us
    <a href="https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html">usage
    ping</a>. We know from a <a href="https://app.periscopedata.com/app/gitlab/545874/Customers-and-Users-with-Usage-Ping-Enabled?widget=7126513&udv=937077">previous
    analysis</a> that only ~30% of licensed instances send us usage ping at least. We are planning to update the methodology to track opt-in rate more accurately. 
    once a month.
  target: 
  org: Product
  public: true
  is_key: false
  health:
    level: 3
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 7441303
    dashboard: 527913
    embed: v2

- name: New hire location factor
  base_path: "/handbook/product/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  target: Our Target is 0.72.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 6838981
    dashboard: 527913
    embed: v2
  sisense_data_secondary:
    chart: 7154820
    dashboard: 551165
    embed: v2

- name: Refunds processed as % of orders
  base_path: "/handbook/product/performance-indicators/"
  definition: For a month take the number of refunds and divide that by the total
    amount of orders (including self-managed and sales initiated orders). More details
    <a href="https://about.gitlab.com/handbook/support/workflows/verify_subscription_plan.html#refunds-processed-as--of-orders">available
    here</a>
  target: 
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 7720228
    dashboard: 527913
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0

- name: Acquisition impact
  base_path: "/handbook/product/performance-indicators"
  definition: Acquisition impact measures the number of <a href="https://about.gitlab.com/direction/maturity/">maturity
    levels</a> advanced, as a result of acquisitions. The target is 3 per year.
  target: 3 per year
  org: Product
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - TBD

- name: Stage Monthly Active Users (SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stage Monthly Active Users is required for all product stages. SMAU
    is defined as the specified AMAU within a stage in a 28 day rolling period 
    for Self-Managed and SaaS. Numbers displayed below are for self-managed instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>), 
    including the self-managed instance hosting the SaaS product. 
    All (<a href="https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0">SMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import.
    Owner of this KPI is the VP, Product Management. 
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
    - The SMAU candidate for each stage is selected by evaluation of Usage Ping data.
    - The SMAU for the Manage Stage (Owner - @jeremy) is the number of unique users 
      viewing any [Analytics page](https://docs.gitlab.com/ee/user/analytics/).
    - The SMAU for the  Plan Stage (Owner - @justinfarris) is the number of unique
      authors who create issues (Issue.distinct_count_by(author_id)).
    - The SMAU for the  Create Stage (Owner - @ebrinkman) is the number of unique
      users who performed a repository write operation.
    - The SMAU for the  Verify Stage (Owner - @jyavorska) is the number of unique
      users who trigger ci_pipelines (Ci.pipeline.distinct_count_by(user_id)).
    - The SMAU for the  Package Stage (Owner - TBD) is in the evaluation phase.
    - The SMAU for the  Secure Stage (Owner - @david) is the number of unique users
      who have used one or more of the Secure scanners. This is tracked with the `user_unique_users_all_secure_scanners`
      statistic in usage ping. [More details on AMAU](/handbook/product/performance-indicators/#action-monthly-active-users-amau)
    - The SMAU for the  Release Stage (Owner - @jreporter) is the number of unique
      users who trigger deployments (Deployment.distinct_count_by(user_id)).
    - The SMAU for the  Configure Stage (Owner - @nagyv-gitlab) will be a proxy metric
      of all the active project users for any projects with a cluster attached. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the  Monitor Stage (Owner - @kbychu) will be a proxy metric of
      all the active project users for any projects with Prometheus enabled. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the Protect Stage (Owner - TBD) is in the evaluation phase.
  sisense_data:
    chart: 10050807
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10050822
    dashboard: 758607
    embed: v2

- name: Paid Stage Monthly Active Users (Paid SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid Stage Monthly Active Users is required for all product stages.
    Paid SMAU is defined as the count of SMAU (Stage Monthly Active Users) that roll
    up to paid instance for Self-Managed using Usage Ping data or a paid namespace for SaaS using GitLab.com Postgres Database Imports in a 28 day rolling period.  
    The license information used to generate this metric is being validated, and the currently displayed data could
    be incorrect by up to 20%.  Numbers displayed below for self-managed are for instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>). 
    (<a href="https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU?widget=9128003&udv=0">Paid SMAU for SaaS</a>)
    is available separately using GitLab.com Postgres Database Import.
    Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - See reasons for [SMAU](/handbook/product/performance-indicators/#stage-monthly-active-users-smau) 
  sisense_data:
    chart: 8887064
    dashboard: 634200
    embed: v2
    
- name: Stage Monthly Active Clusters (SMAC)
  base_path: "/handbook/product/performance-indicators/"
  definition: In categories where there is not a directly applicable SMAU metric as
    the category is focused on protecting our customer's customer, other metrics need
    to be reviewed and applied.  An example of a group where this applies is [Protect's
    Container Security group](/handbook/product/product-categories/#container-security-group)
    where their categories are designed to protect our customers and by extension
    their customers who are actively interacting with our customer's production /
    operations environment.  Metrics such as stage monthly active clusters (SMAC)
    can be used in place of SMAU until it is possible to achieve an accurate metric
    of true SMAU which may include measurements of monthly active sessions into the
    customer's clusters.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Group Monthly Active Users (GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: GMAU is defined as the count of unique users who performed a particular
    action, or set of actions, within a group in a 28 day rolling period. Each R&D
    group is expected to have a primary performance indicator they are using to gauge
    the impact of their group on customer outcomes.  Where possible, groups should
    use Group Monthly Active Users (GMAU) or Group Monthly Active Paid Users (GMAPU)
    as their primary performance indicator.  GMAU should be used when the group is
    early in its maturity and its primary goal is new user adoption.  Paid GMAU should
    be the primary performance indicator when groups are more mature and have proven
    an ability to drive incremental IACV.  We prefer measuring monthly active usage
    over other potential measures like total actions taken (eg total dashboard views),
    as we believe that measuring active usage is a better measure of true user engagement
    with GitLab's product. Since R&D groups often contain more than one category,
    picking one category to base the action, or set of actions on, is a recommended
    simplification as we do not have the data bandwidth to support measuring every
    category's usage at this time.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Secure & Protect Section Performance Indicator Page](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)

- name: Paid Group Monthly Active Users (Paid GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid GMAU is defined as the count of GMAU (Group Monthly Active Users)
    that roll up to a paid instance for Self-Managed or a paid namespace for SaaS in a 28 day rolling period. When Paid GMAU is the
    focus area for a group, they should measure actions tied to`Up-tiered features`,
    which are features that are unavailable in the free tier, as this will ensure
    we are tracking usage that has a clear tie to incremental IACV.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The Paid GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Secure & Protect Section Performance Indicator Page](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)
- name: Action Monthly Active Users (AMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: An action describes an interaction the user has within a stage. The
    actions that need to be tracked have to be pre-defined. AMAU is defined as the
    number of unique users for a specific action in a 28 day rolling period. AMAU
    helps to measure the success of features. Note - There are metrics in usage ping
    under usage activity by stage that are not user actions and these should not be
    used for AMAU. Examples include Groups - `GroupMember.distinct_count_by(user_id)`,
    which is the number of distinct users added to groups, regardless of activity,
    ldap_group_links - `count(LdapGroupLink)`, which is a count of ldap group links
    and not a user initiated action, and projects_with_packages - `Project.with_packages.distinct_count_by(creator_id)`,
    which is a setting not an action.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - Dashboard [Issue](https://gitlab.com/gitlab-data/analytics/issues/1466)
- name: Active Churned User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com user, who is not a UMAU in month T, but was a UMAU in month
    T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health
- name: Active Retained User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com user, who is a UMAU both in months T and T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health
- name: Paid User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com <a href="https://app.periscopedata.com/app/gitlab/500504/Licensed-Users-by-Rate-Plan-Name">Licensed
    User</a>. For Self-Managed (data source - usage ping), a paid user is a user that is on a paid instance. For SaaS, a paid user is a user that is on a paid namespace. 
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1445
- name: Paid UMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#paid-user">paid</a>
    <a href="/handbook/finance/gitlabcom-metrics/index.html#monthly-active-user-umau">UMAU</a>.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1446
- name: Monthly Active Namespace (MAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab <a href="https://docs.gitlab.com/ee/user/group/">Group</a>,
    which contains at least 1 <a href="https://docs.gitlab.com/ee/user/project/">project</a>
    since inception and has at least 1 <a href="https://docs.gitlab.com/ee/api/events.html)">Event</a>
    in a calendar month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1448
- name: Active Churned Group
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com group, which is not a MAG in month T, but was a MAG in
    month T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1449
- name: Active Retained Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace both in months T and T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1450
- name: New Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace created in the last 30 days.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1451
- name: Paid Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace, which is part of a paid plan, i.e. Bronze, Silver
    or Gold. <a href="https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/">Free
    licenses for Ultimate and Gold</a> are currently included.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1452
- name: Paid MAG
  base_path: "/handbook/product/performance-indicators/"
  definition: A <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#paid-group">paid</a>
    <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#monthly-active-group-mag">MAG</a>
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1453
- name: Product Tier Upgrade/Downgrade Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: This is the conversion rate of customers moving from tier to tier
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/3084
- name: Lost instances
  base_path: "/handbook/product/performance-indicators/"
  definition: A lost instance of self-managed GitLab didn't send a usage ping in the
    given month but it was active in the previous month
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1461
- name: User Return Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: Percent of users or groups that are still active between the current
    month and the prior month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1433
- name: Churn
  base_path: "/handbook/product/performance-indicators/"
  definition: The opposite of User Return Rate. The percentage of users or groups
    that are no longer active in the current month, but were active in the prior month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Stage Monthly Active Namespaces (SMAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stage Monthly Active Namespaces is a KPI that is required for all product
    stages. SMAN is defined as the highest AMAN within a stage in a 28 day rolling
    period.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Action Monthly Active Namespaces (AMAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: AMAN is defined as the number of unique namespaces in which that action
    was performed in a 28 day rolling period.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Acquisition success
  base_path: "/handbook/product/performance-indicators/"
  definition: Acquisition success measures how successful GitLab at integrating the
    new teams and technologies acquired. An acquisition is a success if it ships 70%
    of the acquired company's product functionality as part of GitLab within three
    months after acquisition. The acquisition success rate is the percentage of acquired
    companies that were successful. The target is 70% success rate.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - 
- name: Sourced pipeline
  base_path: "/handbook/product/performance-indicators/"
  definition: The sourced pipeline includes companies operating in a DevOps segment
    relevant for GitLab. Companies have developed or are working on product functionality
    which is either a new category on our roadmap or at a more advanced maturity than
    our current categories. Target is ongoing monitoring of at least 1000 companies.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
