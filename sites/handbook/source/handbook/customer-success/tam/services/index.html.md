---
layout: handbook-page-toc
title: "TAM Responsibilities and Services"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## TAM Alignment

Customers who meet the following criteria are aligned with a Technical Account Manager:

### Enterprise

- [Meets Enterprise Segment Criteria](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
- Recurring contract value (ARR) of $50,000 or greater
- Exceptions can be made for clearly defined growth opportunities, decided by sales and TAM leadership

### Commercial

 - Commercial (Mid-Market and SMB) customers are eligible for a TAM if the recurring contract value (ARR) is greater than $50,000.

## Responsibilities and Services

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management

- Regular cadence calls
  - Ask customers about planned upgrades on a regular basis. Refer them to the [Live Upgrade Assistance page](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance) so that they have a plan in place (including rollback strategy) and give Support enough preparation time to be available to help.
- Regular open issue reviews and issue escalations
- Account health checks
- Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
- Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
- To act as a key point of contact for guidance and advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

### Training

- Identification of pain points and training required
- Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
- "Brown Bag" trainings
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

### Support

- Upgrade planning
- User adoption strategy
- Migration strategy and planning
- [Infrastructure upgrade coordination](/handbook/customer-success/tam/services/infrastructure-upgrade)
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations
- Monitor SaaS based customer experience by adding them to the [Marquee Accounts alerts](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts) project



## Coverage

As part of GitLab's [paid time off policy](/handbook/paid-time-off/), team members are encouraged to take time off. When a TAM is out of office, they should rely on their SALs and SAs to lead any customer meetings or respond to requests. Make sure your SAL and SA know in advance that you'll be out and for how long so they can be prepared.

For any specific customer needs (escalating tickets, logging issues, etc.), the TAM should rely on the SA, while the SAL can help with higher-level items.

It's acceptable to cancel or reschedule a weekly cadence call, unless there is an emergency, in which case the SA will help out.

It's possible to ask another TAM to cover for you as well, but they likely don't have the same context as an SA so may require additional briefing and introductions with the customer before you leave.

If your acccounts don't have an SA, you can decide if it would be better to rely solely on your SAL or to bring in another TAM for support, but be sure to arrange coverage in any circumstance and to follow our [PTO communication guidelines](/handbook/paid-time-off/#communicating-your-time-off).

