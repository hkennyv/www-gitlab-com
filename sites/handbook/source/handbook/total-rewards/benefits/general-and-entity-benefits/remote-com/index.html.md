---
layout: handbook-page-toc
title: "Remote.com"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Remote](https://www.remote.com/) and apply to team members who are contracted through Remote. If there are any questions, these should be directed to the Total Rewards team at GitLab who will then contact the appropriate individual at Remote.

## South Africa

### Healthcare Monthly Allowance

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members for their own Medical Coverage.
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.
* Proof of coverage must be shared or forwarded to total-rewards@ domain in order to receive the correct allowance amount. 
* The allowance will be up to R5,000 per month for team member and up to R8,500 per month for team member plus Dependants.

### Discovery Life Provident Umbrella Fund (Provident fund match)
 
Under the Remote Technology scheme, the employer and member both pay **5% each to the Provident fund** as contributions. Applicable tax laws provide that any contribution the employer makes is treated as a contribution made by the member. The contributions will qualify for a tax deduction in each tax year of assessment. 

### Group Life including the Global Education Protector & Funeral Family Benefit

* Sum insured of the Group Life Cover will be 4 times of the annual salary.
* The primary protection offered by group life cover is as follows:
    * Provide for living expenses for surviving dependants
    * Extinguish debt
    * Protect the lifestyle of the surviving dependants
    * Fund for the education costs of the surviving children
* Upon the death of the member, spouse or child, a benefit payment equal to the amount of the Funeral Cover will be made. 
* The sum assured for this benefit is up to R30,000
* This benefit is 100% contributed and covered by Remote.    

### Disability - Income Continuation Benefit

* The Income Continuation Benefit is designed to provide team member with a payment equal to the income they received before they became disabled or severely ill.
* Disability refers to injury, illness or disease that has resulted in a member being unable to perform his or her own job based on objective medical criteria.
* Due to changes in the tax treatment of income protection benefits, the disability income benefit will be calculated on a flat **75% of member salary**, but subject to a maximum of the team members’s net of tax salary or specified rand maximum including any retirement fund waiver benefits.
* The 75% income continuation will come into effect from the **4th month** of the leave.
* This benefit is 100% contributed and covered by Remote.

### Disability - Severe Illness

* A severe illness is an illness that affects a person’s lifestyle in such a way that their ability to function normally is altered.
* Sum assured of this benefit is twice the annual salary of the team member (2 x Annual Salary).
* Discovery Life provides insurance to cover team member against the impact of a severe illness. The Severe Illness Benefit pays a lump sum if a team member is diagnosed with a covered physiological or anatomical severe illness. The claim payment is proportional to the severity of the illness, with severity levels that have been set to reflect the financial impact of the illness on their lifestyle.
* The lump sum benefit provides financial assistance to ensure that the team member can maintain their lifestyle after a life-changing event. This could mean having their homes modified to accommodate their injury or illness, or reinvesting the money to replace the monthly income they can no longer earn
* This benefit is 100% contributed and covered by the Remote.

For more details on benefits managed by Discovery: [Remote Technology Employee Benefits](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/bfd6b16936b7cfaccc8fe0992bd7271f/ZA_Remote_Technology.pdf)

## Mexico

### Social Security

All workers employed in Mexico must be registered with and contribute to the following institutions that deal with different social security insurance benefits:
* Mexican Institute of Social Security (IMSS):
    * Provides medical services, child day care, accident and sickness compensation, pregnancy benefits and disability pensions
* National Workers’ Housing Fund Institute (INFONAVIT):
    * Provides subsidized housing to employees as well as loans, and the Retirement Savings Program (SAR). SAR provides employees with retirement benefits when they reach 65 years of age.

Employees in Mexico are covered by the Social Security Law, under IMSS who is responsible for administering social security insurance benefits and the collection of contributions.
* Both the employer and employee are required to contribute to social security, although the employer has the responsibility of withholding the employee’s contribution.
* These contributions fund retirement pensions, health and maternity insurance, occupational risk, day-care, disability and life insurance, and unemployment/old age insurance.

### Medical Benefits

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members.
* The allowance will be 5000 USD per annum. 
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.

### Life Insurance

* Life insurance cover provided via Remote (Details to be added by Total Rewards)

### Christmas Bonus (Aguinaldo)

* GitLab offers 30 working days pay (which includes total earnings + taxable allowances + commissions)
* Paid by December 20th (so the employee can use it for the holiday)
* Employees with less than one year of service will recieve a pro-rated christmas bonus. 

### Vacation Bonus (Prima)

The vacation premium is an additional cash benefit given to employees for use on their vacation. It is calculated as a minimum of 25% of daily salary multiplied by the number of days of vacation. Employees who have provided one year of service must be afforded a minimum of 6 paid vacation days in their first year of employment. Two working days will be added to that vacation time every following year through the fourth year. After five years, employers are required to add two days of vacation time every five years.
* Here is a chart on how vacations days increase:

| Year(s)  | Days |
|----------|------|
| 1        | 6    |
| 2        | 8    |
| 3        | 10   |
| 4        | 12   |
| 5 to 9   | 14   |
| 10 to 14 | 16   |
| 14 to 19 | 18   |

**Calculation:**
* To calculate the vacation bonus based on 25% and a salary of $50,000 USD per year:
* $50,000 USD divided by 365 days = $136.99 USD daily pay rate
* $136.99 USD daily pay rate multiplied by 25% = $34.25 USD
* $34.25 USD multiplied by 6 vacation days (first year) = $205.50 USD vacation bonus
* It can be paid right away when the vacation is taken, or as a lump sum upon completing one year of service (or employment ends). It is the employer’s choice which method to use.
* Unused leave must also be paid when employment ends (in other words, the employee does not ‘forfeit’ unused vacation time)

### Remote.com Mexico Leave Policy

#### Statutory Maternity Leave

The statutory entitlement for maternity leave is 12 weeks (6 weeks before the child is born and 6 weeks after birth). The employee can negotiate the leave period before the birth of the child in an agreement with the employer in order to partially enjoy them after the birth of the child. During maternity leave, women are entitled to the benefits that they would normally receive.

* In addition to the following rules apply: 
    * Pregnant women may not work under the hazardous conditions; 
    * Perform industrial tasks during the night, extraordinary hours or during sanitary contingencies; 
    * Their salary, benefits, and rights should not be affected.
* When an employee return from maternity leave, the employee is entitled to her employment, provided that no more than 1 year has passed since the date of delivery.
* Maternity leave does not affect the longevity of service. During maternity leave, the Mexican Social Security Institute will pay the working mother 100% of her daily salary as a social security contribution.
* If the maternity leave period is extended, she is entitled to 50% of the daily salary of social security contribution for a period of up to 60 days.

#### Statutory Paternity Leave

A male team member is entitled to 5 working days as paternity leave.

### Sick Leave

Team members unable to work because of a nonwork-related injury or illness and who have made payments into the social security system for the four weeks before the condition developed are eligible for paid sick leave through the Social Security Institute. The benefit, which is 60% of an employee’s regular wage, is paid from the fourth day of the illness for up to 52 weeks and maybe extended for another 52 weeks. 

## Hungary

### Social Security

The Hungarian Social Security Act has employer and team member contributions to cover statutory requirements such as pension, health insurance, and unemployment. 

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Hungary can access the public health system for free or at a lower cost through the Hungarian state healthcare system. This system is funded by the National Health Insurance Fund.

### Pension

GitLab does not plan to offer pension benefit at this time as the Hungarian pension system provides for a minimum pension, with a qualifying condition of minimum 20 years of service, of HUF 28,500 per month. If the average contribution base is less than the amount of the minimum pension, the pension will equal 100% of the average monthly wage.

### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from [Social insurance system](https://tcs.allamkincstar.gov.hu/) if they get ill, injured or have a disability.

### Remote.com Hungary Leave Policy

#### Statutory Maternity Leave

The statutory entitlement for maternity leave is 24 weeks. The leave must start **4 weeks prior to the scheduled delivery date**. During the entire period of maternity leave, the team member is entitled to 70% of her average salary, if she is regarded as insured under the Health Insurance Act LXXXIII. of 1997.

#### Statutory Paternity Leave

A male team member is entitled to `five` days paid leave, and `seven` days paid leave in case of twins, which they may take in one or in several parts until the end of the second month after the birth. Any income and taxes related to this period of supplementary leave are reimbursed to the employer by the Hungarian State Treasury. (Labour Code §118)

#### Statutory Parental Leave

The team member shall be entitled to a leave of absence without pay in order to care for the child until the child reaches the age of `three` or in order to care for the child until the child reaches the age of `ten`, if the team member receives a child care allowance. (Labour Code §128, 130, 118)

Additionally to parental leave, workers are entitled to extra vacation days based on the number of children they have: 
a) two working days for one child; 
b) four working days for two children; 
c) a total of seven working days for more than two children under sixteen years of age.
