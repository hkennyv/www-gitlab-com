---
layout: handbook-page-toc
title: "Application Security Stable Counterparts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Stable Counterparts

The overall goal of Application Security Stable Counterparts is to help
integrate security themes early in the development process. This is intending
to reduce the number of vulnerabilities released over the long term.  
These Stable Counterparts can be found on the 
[Product Categories page](/handbook/product/product-categories/#devops-stages), 
next to the "Application Security Engineer" mentions.

### Technical Goals
- Assist in threat modeling features to identify areas of risk prior to
  implementation
- Code and [AppSec reviews](/handbook/engineering/security/application-security/appsec-reviews.html)
- Provide guidance on security best practices
- Improve security testing coverage
- Assistance in prioritizing security fixes
- Provide technical guidance on security fixes

### Non-technical Goals
- Enable development team to self-identify security risk early
- Help document and solve pain points when it comes to security process
- Identify vulnerability areas to target for training and/or automation
- Assist in cross-team communication of security-related topics
- Assist development teams with security related compliance and regulatory audits
