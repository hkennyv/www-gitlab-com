---
layout: handbook-page-toc
title: "Problem validation for multi-stage-group initiatives"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Multi-stage-group research initiatives focus on topics that impact two or more [stage groups](https://about.gitlab.com/handbook/product/product-categories/#devops-stages). By conducting research that spans stage groups, it allows us to consider the holistic user experience, helping UX research and design to support the goals of immediate and adjacent stage groups, the broader product vision, and company objectives.

**Note:** A Lead UX Researcher is the person who devises the research brief and provides an initial outline of the study's goals and hypotheses. A Lead UX Researcher can hold any level of seniority and experience, from Research Coordinator to Staff UX Researcher. The Lead UX Researcher is the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) for the research study. They should communicate that they are the DRI during stakeholder meetings and within research issues.
1. Lead UX Researcher meets with the relevant PM(s), fellow UX Researcher(s), Research Coordinator(s), and UX Research Manager to discuss the goals of the study and the hypotheses they have. The purpose of this meeting is to get buy-in from all stakeholders. If a stakeholder is unable to attend the meeting, record the session and offer the opportunity to provide feedback asynchronously.
1. Lead UX Researcher creates an issue using the [Problem Validation template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Problem_Validation.md).
1. Lead UX Researcher applies the ~"workflow::problem validation" label to the associated issue, which automatically removes the ~"workflow::validation backlog" label.
1. Lead UX Researcher completes an [opportunity canvas](https://about.gitlab.com/handbook/engineering/ux/ux-research/#opportunity-canvas) to the best of their ability. Ensure the problem and persona are well articulated and add the opportunity canvas to the issue's [Designs](https://docs.gitlab.com/ee/user/project/issues/design_management.html#the-design-management-page). Note that you should include content for the solution and go-to-market sections, possibly with low confidence. This section is likely to change, but thinking it through will help clarify your thoughts.
1. Lead UX Researcher opens a problem validation research issue using the available template in the UX Research project, completes it, and assigns the issue to the relevant UX Researcher(s).
1. Follow the steps outlined for [user interviews](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/) or surveys. However, for multi-stage-group initiatives, the Lead UX Researcher should take responsibility for all steps outlined in the user interview or survey process -- for example, writing a discussion guide. They are welcome to delegate responsibilities to other UX Researchers, if they need assistance.
1. Lead UX Researcher meets with the relevant PM(s), UX Researcher(s), and UX Research Manager to discuss findings and next steps.
The Lead UX Researcher may be required to:
    * Finalize the opportunity canvas with the synthesized feedback.
    * Present the opportunity canvas to Scott Williamson, Christie Lenneville, and the relevant Product Director(s).

