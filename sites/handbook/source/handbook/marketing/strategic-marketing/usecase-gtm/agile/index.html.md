---
layout: markdown_page
title: "Agile"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

| PMM | TMM |
| --- | --- |
| Cormac Foster ([@cfoster3](https://gitlab.com/cfoster3)) | Tye Davis ([@davistye](https://gitlab.com/davistye)) |
| Brian Glanz ([@brianglanz](https://gitlab.com/brianglanz)) |  |

# The Market Viewpoint

## Agile Planning and Management
By empowering teams, embracing change, and focusing on delivering value, Agile methodologies have transformed software development. Agile teams create more relevant, valuable, customer-centric products, more quickly than ever.

Development teams accelerate the delivery of value with iterative, incremental, and lean project methodologies including Scrum, Kanban, Extreme Programming (XP), and more. Large enterprises have adopted Agile at enterprise scale through a variety of frameworks, including Scaled Agile Framework (SAFe), Spotify, Large Scale Scrum (LeSS), and others. GitLab enables teams to apply Agile practices and principles to organize and manage their work, whatever their chosen methodology. These new methodologies bring new challenges.

Agile is not a single methodology, but a set of guiding principles embraced by many methodologies in many ways. There is no single “best” way to plan and manage an Agile enterprise, and older project planning tools are often insufficiently flexible to address the needs of a given implementation.

This functionality gap leads to poor project estimation, planning, and progress tracking. Furthermore, Agile methodologies can create confusion between teams and throughout the enterprise, where even well-documented Agile processes may not map to traditional success metrics and waterfall financial planning.

Oragnizations need better ways to manage projects, programs, and portfolios using Agile. They need to better:

* **plan** and prioritize work based on strategic objectives
* **initiate** work
* **monitor** the progress of that work in progress
* **collaborate** on work, across teams, throughout the product lifecycle
* **control** and optimize the flow of work
* **close** work items when completed
* **measure** the value created by that work
* **communicate** that value throughout the organization, regardless of other methodologies in place

## Total Addressable Market
GitLab has published research on [the TAM and broader market for our Plan stage](/direction/plan/#the-plan-market), which we reuse here as a proxy to MVC the Agile Use Case TAM:

> After reviewing reports and analysis from numerous sources we believe the Total Addressable Market(TAM) for Plan is $14.8B in 2020, growing to $20.0B by 2025 - a Compounded Annual Growth Rate (CAGR) of 10.7%. This encompasses tooling across the entire Plan product segment from task tracking and bug tracking, to enterprise planning tools and Application Lifecycle Management (ALM). These segments represent 48% of GitLab’s estimated Serviceable Addressable Market (SAM) and 40% of our TAM; which is not inclusive of the rest of the DevOps lifecycle tooling such as SCM, CICD, or other related tools.
>
> As GitLab continues to mature and gain market share across our existing Stages, we see a strong opportunity to extend the value proposition of a “single application for the entire DevOps lifecycle” into the $35B Enterprise Resource Planning (ERP) and $48.2B Customer Relationship Management markets.

There are many ways to plan and develop software that are not Agile — and GitLab Plan is intentionally flexible enough to support methodologies from waterfall to DevOps, regardless of whether one or another flavor of Agile is entirely or partially adopted. This is to say that the TAM for Plan is not the same as the TAM for the Agile Use Case, even as they overlap enough that we're going with the substitution for now.

GitLab also recently published market research for the section containing our Plan stage, with [this December, 2019 review of our Dev strategy](/blog/2019/12/04/dev-strategy-review/). That analysis includes an estimate of TAM for the combined Manage, Plan, and Create stages, but with an estimate of $3B in 2019, growing to $7.5B in 2023 (26.5% CAGR). As this estimate is a little older, we'll favor the Plan stage estimate but note the significant discrepancy.

## Market Leaders

### GitLab
[GitLab was named a 'Visionary' by Gartner in their 2020 Magic Quadrant for Enterprise Agile Planning Tools](/blog/2020/08/03/gitlab-named-visionary-in-gartner-agile-planning-magic-quadrant/). It was the second consecutive year Gartner recognized GitLab as a Visionary, despite [2019 being our first year in the MQ](/blog/2019/05/22/gitlab-identified-by-gartner-as-eapt-visionary/) and despite our being a fairly new entrant into the space.

_"Magic Quadrant for Enterprise Agile Planning Tools," Mann et al., 21 April 2020. Gartner does not endorse any vendor, product or service depicted in its research publications and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s Research & Advisory organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose._

### Atlassian
Average annual revenue growth of 40% over the last five years, $1.2B total revenue in 2019, and demand for remote working capabilities and SaaS during the global pandemic have propelled Atlassian (TEAM) to a $42B market capitalization.

Atlassian has consistently increased its market footprint through strategic acquisitions that would allow its complimentary products to organically self-propagate through enterprises. At its Remote Summit in April, 2020, Atlassian announced many new capabilities in Jira Align, based on their $166 million acquisition of AgileCraft. AgileCraft, the company and the software, was a former market leader with support for SAFe and other enterprise Agile frameworks; it also featured strong support for teams using Atlassian Jira.

While Atlassian have many products, those most aligned with the Agile Use Case include Jira Software (Software Project Management), Jira Align (Portfolio Management), Confluence (Wiki), Trello (Project Management), Jira Core (Business Project Management), and Jira Service Desk (ITSM Software).

### CollabNet VersionOne
In 2019, TPG Capital purchased CollabNet VersionOne from Vector Capital as part of TPG’s $500M equity capital investment to build an integrated DevOps company called Digital.ai, which in 2020 also includes XebiaLabs and Arxan Technologies. Digital.ai provides end-to-end solutions for Global 5,000 enterprises.

Agile planning tools VersionOne and Continuum support Scrum, Kanban, Extreme Programming (XP), and SAFe. Value stream management and portfolio management have been more of a focus, and VersionOne is available as SaaS or on-prem. DevOps integrations include Jenkins, Chef, and others.

### Broadcom Rally
As [GitLab's Plan stage competitive landscape section](/direction/plan/#competitive-landscape) describes:

> Rally was the de facto market leader at one point in time, but has seen its market share decline substantially. While it is still entrenched in many enterprises, many speculate that Broadcom has placed Rally on life support.

Rally is part of Broadcom’s Digital BizOps Starter Edition and benefits from the company's larger presence, history, and product portfolio. Broadcom had acquired CA in 2018, for some time after which this product was still branded CA Agile Central.

### Planview
Planview provides a range of enterprise grade project, product, and portfolio management capabilities. Lead by investments from Venture Partners in 2013 and Thoma Bravo in 2017, Planview has been steadily acquiring competitors such as Troux Technologies, Innotas, LeanKit, and Spigit to expand its reach into different markets.

The product, Planview Lean and Agile Delivery, is said to have strong support for SAFe, Kanban, and Scrum, with Kanban useful outside of software development including portfolio management and business strategy.

### Targetprocess
As [GitLab's Plan stage competitive landscape section](/direction/plan/#competitive-landscape) describes:

> One of the smaller and younger competitors in the plan space, Targetprocess has rapidly increased its market footprint and has been recognized by Gartner as a leader in the EAPT market.

### Microsoft
As [GitLab's Plan stage competitive landscape section](/direction/plan/#competitive-landscape) describes:

> With the acquisition of GitHub, Microsoft has started the process of combining the best from one of the world’s most popular SCM solutions with key features from Azure DevOps. Microsoft Project is their leading plan solution that currently owns 15-25% of the planning market.

### ServiceNow
While they are an established vendor in IT service management, ServiceNow's Agile planning tool, the IT Business Management (ITBM) suite is said to have relatively basic functionality. ITBM includes release planning, limited SAFe support, and Jenkins integration, while its main advantage would be integration with the Now Platform and generally unifying development and operations backlogs via ServiceNow.

## Trends
* Toward portfolio planning in addition to project planning
* Increased importance of visibility and value stream management
* Enterprise methodologies like SAFe and the Spotify model become more mainstream
* Continued expansion of the Agile use case beyond software development
* Increased importance of coherence with compliance and business leadership
* Increased importance of Agile tools producing, integrating with, and being guided by data

## SWOT Analysis
### Strengths
We aim to draw on the strengths of a single DevOps platform by creating a unified approach to key and emerging aspects of the Agile use case such as requirements management and quality management.

Our transparency and accordant willingness to be direct with, and meaningfully involve customers.

GitLab's own modern but flexible software, from basis in Git to cloud integrations, positions us for the future and to be agile in our support of customers adopting Agile.

The growth of DevOps is concurrent and may be symbiotic with that of Agile, and our reputation in DevOps precedes us.

### Weaknesses
GitLab has less to offer in Agile beyond project or product management, which is also a trending area for the use case.

Our MVC approach to developing and deploying GitLab can make a poor first impression on prospective customers from more traditional, less Agile organizations.

We're a relatively new player in the market, with immaturity such as less specifically to offer an important sub-use case like SAFe, less robust use of data.

### Opportunities
GitLab is positioned for the future, with points including our ascendancy in DevOps, an emerging future in which every company will need a DevOps platform and the trend toward platforms over point solutions, our strength in DevOps for enterprise customers, and our own modern approach and architecture, optimized for cloud native applications.

### Threats
The mergers and acquisition landscape indicates that competitors are working on building or acquiring end-to-end platforms like GitLab, potentially mitigating against a key differentiator.

Larger players such as Microsoft and Atlassian have resources enabling them to close or open gaps quickly in Agile and adjacent areas.

As Agile becomes ever more mainstream, expansion may become more difficult per more stubborn shadow IT and resistance to change.

## Personas

### User Personas
With GitLab's unique capability to connect every phase of the Software Development Lifecycle in a single DevOps platform, many user personas find a solution in Integrated Agile Planning.

User personas and their key motivations for using GitLab for Agile planning include:

#### [Parker the Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
- PMs coordinate feature development and project success, along with general workload. Abilities to monitor progress through commits and the review app, and to validate those changes and provide feedback, are key to their success.
- Over time, changes generate valuable statistical insight for PMs to assess progress with product development and other projects.

#### [Delaney the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
- Like PMs, Team Leads need to understand their team's capacity to assign upcoming tasks and meet goals on time.

In consideration: personas from an Agile Planning perspective might include and be named as:
* Scrum Master
* Product owner
* Developer
* Tester/QA
* Project managers
* Product managers

### Buyer Personas
Purchasing for Integrated Agile Planning may not require executive involvement, with one possible exception being the influence of the [VP Application Development](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager).

#### [Erin the Application Development Executive (VP, etc.)](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)
- Erin is a strategic leader focused on business challenges and the big picture.
- Erin's top goal is predictable business results.

## Industry Analyst Resources
List key analyst coverage of this use case


# Market Requirements
**Map business and portfolio plans to market opportunities**

| Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| 1. Requirements modeling, management, and analysis  |  Describe necessary product behavior against which output can be validated.  |  Epics, Features, Stories, Tasks, Health Status Reporting, and any necessary compliance to manage requirements for regulated industries |  Provides a common vision for the product's end state and an archive of scope changes throughout the development process, Ensures that value being delivered meets stated goals and necessary compliance.  |  
| 2. Flexible workflow support  |  Enables events and workflows from popular Agile frameworks and custom Agile implementations  |  Sprint planning, sprint retrospectives, customizable timeboxes, support for enterprise Agile frameworks such as SAFe and LeSS  |  Increases productivity by reducing context switching, provides a single source of truth for onboarding, auditing, and reference, removes operational resistance to Agile adoption by working with existing methodologies.  |  
| 3. Value Stream Management  |  Visualize an end-to-end workstream, identify and target waste and inefficiencies, and tune those workstreams to deliver the highest possible velocity of customer value.   |  Custom workflow modeling and measurement, waste identitifcation  |  Provides an objective standard by which the effectiveness of efforts can be judged and identifies opportunities for high-impact efficiency improvements.  |  
| 4. Role-specific interfaces  |  Interfaces that surface relevant information in a context suited to different users.  |  Boards, roadmaps, dashboards  |  Allows workers to inspect and interact with workflows without switching context from their primary work tools or sifting through information that is not necessary within their business context.  |
| 5. Actionability  |  Tactical participation in workstreams from any part of the management system  |  Inline collaboration, drill-downs from dashboards into work items  |  Closes the loop between ideation and iteration, allowing decision-makers to execute immediately and unblock value flows.  |  
| 6. Traceability  |  Understand the history and context of every action  |  Event logs, exceptions, approval-related conversations  |  Provides an unimpeachable source of truth for audits and retrospectives, verifies that customer value was delivered as requested.   |  
| 7. Quality metrics  |  Usability and value of outputs  |  Test coverage, escape defect analysis, security vulnerabilities, license compliance  |  Improve product value, identifies opportunities for training and process refinement, minimize adverse business and compliance impacts of changes.  |  
| 8. Operational metrics  |  DevOps efficiency and success  |  Deployment Frequency, Lead Time, Cycle Time, Change Failure Rate  |  Identify waste, optimize delivery of value |  
| 9. Organizational effectiveness metrics  |  Efficiency with which the organization collaborates to deliver customer value  |  Team adoption of Agile processes, time tracking, financial / cost tracking and estimation  |  Surfaces systemic process inefficiencies and opportunities for improvement.  |  
| 10. Integrations to external data sources |  Provides visibility into and updating of external systems that contain data to management and planning  |  DevOps, financials, performance monitoring systems  |  Enables tracking of value that extends beyond scope of planning tool, storage of data in other systems for further analysis  |  


# The GitLab Solution

## How GitLab Meets the Market Requirements

| Market Requirements | How GitLab Delivers | GitLab Category | Demos |
| ------ | ------ | ------ | ------ |
| 1. Requirements modeling, management, and analysis  |  Allows users to set criteria against which they can validate products using requirements—artifacts in GitLab that describe the specific behavior of a product. Requirements can be [marked satisfied by a CI job](https://docs.gitlab.com/ee/user/project/requirements/#allow-requirements-to-be-satisfied-from-a-ci-job), and if they are no longer needed, requirements can be [archived](https://docs.gitlab.com/ee/user/project/requirements/#archive-a-requirement). When a feature is no longer necessary, you can archive the related requirement. |  [**Plan stage:**](/stages-devops-lifecycle/plan/) [Requirements Management](https://about.gitlab.com/direction/plan/requirements_management/) |  [GitLab 12.10 introduces Requirements Management](https://www.youtube.com/watch?v=uSS7oUNSEoU)  |  
| 2. Flexible workflow support  |  Provides multiple methods of timeboxing work, allowing users to manage tactical work in Iterations while simultaneously managing strategic efforts across multiple iterations in Milestones. GitLab’s ability to nest multiple levels of sub-Epics allows users to map to any necessary organizational structure, including those of enterprise frameworks such as SAFe.  |  [**Plan stage:**](/stages-devops-lifecycle/manage/) [Epics](https://docs.gitlab.com/ee/user/group/epics/), [Iterations](https://docs.gitlab.com/ee/user/group/iterations/), [Milestones](https://docs.gitlab.com/ee/user/project/milestones/) |    |  
| 3. Value Stream Management  |  Provides out-of-the-box Value Stream Analytics to automatically benchmark the DevOps lifecycle and identify inefficiencies. Allows users to define and measure their own custom lifecycle stages. Insights provides the ability to track and manage custom workflows using Scoped Labels.  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#value-stream-analytics), [Insights](https://docs.gitlab.com/ee/user/project/insights/#insights).<br>[**Plan stage:**](/stages-devops-lifecycle/plan/) [Scoped Labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) |    |  
| 4. Role-specific interfaces  |  Offers a variety of interfaces aimed at role-based contexts, including Security Dashboards, Roadmaps, Value Stream Analytics, Group- and Project-level boards, and aggregation of all relevant information in the Merge Request.  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#value-stream-analytics)<br>[**Plan stage:**](/stages-devops-lifecycle/plan/)[Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/#roadmap), [Boards](https://docs.gitlab.com/ee/user/project/issue_board.html)<br>[**Secure stage:**](/stages-devops-lifecycle/secure/) [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/). |    |  
| 5. Actionability  |  Offers a single system for all work and complete traceability, from planning to collaboration to code changes and their impact on quality and performance. From any point in the application, GitLab users can always drill down into Issues and Merge Requests to collaborate.   |  [**Plan stage:**](/stages-devops-lifecycle/plan/)  <a href="https://docs.gitlab.com/ee/user/project/issues/index.html#issues">Issues</a>, <a href="https://docs.gitlab.com/ee/user/project/merge_requests/">Merge Requests</a> |    |  
| 6. Traceability  |  All actions within GitLab are logged and surfaced in multiple contexts, allowing users to discover and reference historical data with minimal work. Merge Requests aggregate all related information, allowing users to view the collaboration leading to a code change, the impact of a code change, and all relevant collaboration that led to the change and its aproval.  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [Project Import / Export](https://docs.gitlab.com/ee/user/project/settings/import_export.html)<br>[**Plan stage:**](/stages-devops-lifecycle/plan/) <a href="https://docs.gitlab.com/ee/user/project/merge_requests/">Merge Requests</a><br>[**Secure stage:**](/stages-devops-lifecycle/secure/) <a href="https://docs.gitlab.com/ee/user/application_security/security_dashboard/">Security Dashboard</a> |    |  
| 7. Quality metrics  |  Run and view the output of automated tests in your CI pipelines to verify/validate code pre-production. Includes Unit tests, integration testing, browser performance testing, code quality, code coverage, usability testing, and accessibility testing. Automatically scan license compliance on each commit.  |  [**Verify stage:**](/stages-devops-lifecycle/verify/) [CI](https://docs.gitlab.com/ee/ci/), [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html), [Code Testing and Coverage](https://docs.gitlab.com/ee/ci/unit_test_reports.html), [Web Performance](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html), [Usability Testing](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews), [Accessibility Testing](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html)<br>[**Secure stage:**](/stages-devops-lifecycle/secure/) [License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html)  |    |  
| 8. Operational metrics  |  Value Stream Analytics provides insights into how efficently your DevOps processes are running and identifies opportunities for improvement.  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [DevOps Report](https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html) |    |  
| 9. Organizational effectiveness metrics  |  The DevOps Report provides an overview of your entire instance’s adoption of Concurrent DevOps, from planning to monitoring.  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#value-stream-analytics) |    |  
| 10. Integrations to external data sources |  GitLab offers dozens of integrations and an open core model that encourages others [to contribute](https://docs.gitlab.com/ee/user/project/integrations/overview.html#contributing-to-integrations).  |  [**Manage stage:**](/stages-devops-lifecycle/manage/) [Integrations](https://docs.gitlab.com/ee/user/project/integrations/overview.html) |    |  


## Top 3 Differentiators and Key Features

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
|  **Product Development Management**  | GitLab is the only product that provides collaboration capabilities to Product teams that works not only with source code but also IP, graphic assets, animations, binaries, and general project management issues. | Forrester's [Adopt Product Management to Connect Design and Development](https://www.forrester.com/report/Adopt+Product+Management+To+Connect+Design+And+Development/-/E-RES149995) states "Siloed Design And Dev Teams Deliver Subpar Software"  |
| second differentiation  |  def  |  ghi  |
| **Single Application**  |  def  |  ghi  |

## [Message House](./message-house/)
The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

### Discovery Questions
- list key discovery questions

## Competitive Comparison
TBD - will be a comparison grid leveraging the capabilities

### Industry Analyst Relations (IAR) Plan (UPDATE AS NEEDED FOR THIS USE CASE)
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](/handbook/marketing/strategic-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-industry-analyst-interactions).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Proof Points - customers

### Quotes and reviews
- List of customer quotes/reviews from public sites

### Case Studies
- List of case studies

### References to help you close
- Link to SFDC list of use case specific references

## Adoption Guide
### Playbook Steps

- To be added...

### Adoption Recommendation
This table shows the recommended use cases to adopt, links to product documentation, the respective subscription tier for the use case, and product analytics metrics.

| Feature / Use Case                                  |    F/C    |   Basic   |    S/P    | G/U  | Notes                      | Product Analytics |
| --------------------------------------------------- | :-------: | :-------: | :-------: | :--: | :------------------------- | :-------: |

### Enablement and Training

### Professional Service Offers

## Partners
- Describe how key partners help enable this use case

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Direction
- Describe a vision for compelling future improvements

## Resources
### Presentations
* LINK

### Whitepapers and infographics
* LINK

### Videos (including basic demo videos)
* LINK

### Integrations Demo Videos
* LINK

### Clickthrough & Live Demos
* Link

## Buyer's Journey
Inventory of key pages in the buyer's Journey

| **Awareness** <br> learning about the problem  |  **Consideration** <br> looking for solution ideas  |  **Decision** <br> is this the right solution|
| ------ | -------- |-------- |
| [topic page?]()  | [solution page]() | [proof points]() |
| [landing pages?]() | ?comparisons?  | [comparisons]() |
| -etc?            |   |  - [product page x]() <br>  - [product page y]() <br>  - [product page z]() |
