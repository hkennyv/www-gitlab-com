---
layout: handbook-page-toc
title: "GitLab Speaker's Bureau"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is the Speakers Bureau?    

The Speakers Bureau is a group of GitLab team members and members of the wider GitLab community who are available to participate in events and deliver talks about GitLab, CI/CD, open source, remote work, and other topics. 

## Find a speaker  

A list of speakers bureau members can be found on our [Speakers Bureau page](/speakers/). After identifying a speaker for an event or other speaking opportunity, please follow the instructions on the Speakers Bureau page. 

## Join the Speakers Bureau

If you would like to join the speakers bureau, we ask that you submit an MR to add yourself to the [Speakers Bureau page](/speakers/). This can be done by adding the following lines to the speakers.yml file in the www-gitlab-com project.

To create an MR for the Speakers Bureau page, you will need:

- Your personal Twitter / GitLab handles
- Links to recent, relevant presentations or talks you have delivered

Once you have the above items, follow these steps to add yourself to create your MR:

1. Go to the [speakers.yml file in the GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/speakers.yml) project.
2. On the file page, click on the button labeled `Web IDE` near the middle of the page.
3. For community members: if prompted, click `Fork` to create a Fork of the repo which will allow you to make changes and submit a Merge Request.
4. You should see the `speakers.yml` file open in your browser. Add the following fields to the end of the file and enter your information into each of the blank fields:

``` yaml
- name: Ada Lovelace          # Your full name
  region: North America       # One of North America, South America, Europe, Asia, Africa, Oceania
  location: London, England   # What you want your location to be shown as
  tagline: English mathematician and writer, chiefly known for inventing computer programming. # A quick 1 or 2 sentence description of your self and your speaking credentials.
  twitter: olearycrew         # Your twitter handle
  gitlab: brendan             # Your GitLab user name
  affiliation: community      # One of teammember, heroes, or community
  image: ada.png              # Place an image that is 344 × 201 into /source/images/speakers/ 
  topics:                     # One or more of these topics
#    - Remote Work
#    - CI/CD
#    - Git
#    - Security
#    - DevOps
#    - Open Source
#    - Engineering
#    - SRE / Operations
#    - Kubernetes
#    - Monitoring & Observability
#    - UX Design
  presentations:              # A list of previous presentations for the back of the card
#    - title: Foo
#      link: https://
```

> ``` markdown
> Notes: 
>- For `topics`, please only list topics from the following (case-sensitive) list: 
>  - Backend Engineering
>  - Build Engineering
>  - CI/CD
>  - Documentation
>  - Frontend Engineering
>  - Git
>  - Golang
>  - Kubernetes
>  - Leadership
>  - Linux
>  - Monitoring
>  - Open Source
>  - Remote 
>  - Support Engineering
>  - UX Design
>- For `region`, please only include regions from the following (case-sensitive) list: 
>  - Africa
>  - Asia
>  - Europe
>  - Global
>  - North America
>  - South America
>  - US East
>  - US South
>  - US West
```

5. Once you have finished adding your information, click the `Commit` button in the bottom left. It should say something like `1 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
6. Check the file to ensure your updates are what you expect. If they are, click the check mark next to the filename to "stage" these changes.
7. Once you have verified all of the edits, enter a short commit message including what you've changed. Choose `Create a new branch`. Name the branch in the format of `YOURINITIALS-speakers-bureau` or similar. Tick the `Start a new merge request` checkbox. Then click `Commit` once more.
8. Click on the Activity link in the header to go to your Activity page. Once there, click on the blue `Create merge request` button at the top of the page.
9. Fill out the merge request details. Community members who are applying should tick the box to `Allow commits from members who can merge to target branch` as detailed on the [Allow collaboration on merge requests across forks](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members) page in our docs.
10. Team members should assign to `@johncoghlan` and community members should mention `@johncoghlan` in a comment in the merge request so our team can review and merge.
