---
layout: handbook-page-toc
title: "GitLab Offboarding"
description: "Offboarding procedures for all stakeholders"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Offboarding 

The offboarding process execution is shared by the People Business Partners (PBPs), People Specialists, People Experience team, and Tech Stack Provisioners. 

The People Experience team is responsible for creating the actual [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/offboarding.md) within 12 hours of the offboarding date (may be sooner once the team member in the rotation becomes available) and executing the logistical tasks to remove access from certain systems and eliminate specific information in the handbook, etc. as outlined [here](/handbook/people-group/offboarding/offboarding_guidelines/), while the PBPs and People Specialists collaborate to work through the rest of the process.

## Compliance

The People Experience Associates complete a weekly audit of all offboarding issues opened within that specific week and check that all tasks have been completed by all Departments. In the event that tasks are still outstanding, the People Experience Associate will ping the relevant Team Members / Departments within the offboarding issue to call for tasks to be completed.

Once all tasks have been completed, the People Experience Associate will close the offboarding issue and mark as completed in the offboarding tracker.

All offboarding tasks by all Departments need to be completed within 5 days of the offboarding due date. For systems that are more critical and time sensitive, these will be completed within the first 24 hours (example 1Password, Okta, Slack) by the relevant Departments. Information about application & system deprovisioners can be found on the [Tech Stack Applications](/handbook/business-ops/tech-stack-applications/) handbook page.

## Voluntary Offboarding

A voluntary offboarding occurs when a team member informs his or her manager of a resignation. The choice to leave GitLab was their decision.

If you are a current team member and you are thinking about resigning from GitLab, we encourage you to speak with your manager, your assigned PBP, or another trusted team member to discuss your reasons for wanting to leave. At GitLab we want to ensure that all issues team members are facing are discussed and resolved before a resignation decision has been made.  This will allow GitLab the ability to address concerns and in return foster a great work environment.

If resignation is the only solution after you have discussed your concerns, please communicate your intention to resign to your manager. We would advise you to review your employment contract for the statutory notice period and determine your last working day. Then with your manager you can discuss the time needed to work on a handover/transition. If there’s no notice period included in your employment contract we would advise that you provide GitLab 2 weeks of notice. Depending on the situation and local laws, GitLab may choose to provide you payment in lieu of notice.  Hereafter please review the below process which will be followed for a voluntary resignation. 

### Voluntary Process

1. ***Team Member***: Team members are requested to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. ***Team Member***: The team member should provide a written resignation letter or email notification to their manager.
1. ***PBP***: Upon receipt of the resignation, the manager will notify the People Business Partner (PBP) by sending a copy of the resignation email/letter.
    * A discussion with the  manager and PBP should also happen if needed to determine what led up to the resignation.
    * The PBP will acknowledge the receipt of the resignation with the team member confirming the last working day. Feel free to reference this [example email](https://docs.google.com/document/d/1rZvczqEuyyFDAzjheiF4LpcLuvj9fFd6maPOSDwkYpQ/edit).
    * **If the team member is located in a country with statutory holiday allowance**, the holiday taken should be confirmed with the manager and team member via email and then filed in BambooHR.
    * Once that has been done an Acknowledgement Letter can be prepared, which will depend on the `location` and `employment status` of the team member.
    * A couple of examples can be reviewed here: [GitLab Ltd (UK) Resignation Acknowledgement](https://docs.google.com/document/d/1zV1qnZmjQaNZ3QUjrLOtod-FbboNCPmYdqCLIAc7aos/edit), [GitLab BV (Netherlands) Resignation Acknowledgement](https://docs.google.com/document/d/1-9hCL2Xs5po4lZ19L2vrcmGxQIYRD-Emyci3F63kt0c/edit) and [GitLab/Lyra Relieving Letter for India](https://docs.google.com/document/d/1uPlL_l8N-WJXZu2ISNh--eWCY2aB9bSqt9zaj-jmh6w/edit)

    _Note: For GitLab UK team members, payroll can pay up to and including the 5th day of the following month. For example, if a team member's last day is February 2nd, the team member can receive their final pay for this in January's payroll._

1. ***PBP***: The PBP will also inform the team member that they will receive and invitation in the next 48 hours from CultureAmp to complete an exit survey.
1. ***PBP***: PBP will forward the resignation email to the People Experience team at `people-exp@gitlab.com`, the People Operations Specialist team email inbox `peopleops@gitlab.com`, as well as to the payroll lead. The PBP will indicate the last day, reason for resignation, and rehire eligibility in the email. 
_Note: If the team member has a contract with a co-employer, the payroll lead will forward the email to the contact at the co-employer._
1. ***Specialist***: The People Specialist will save a pdf of the resignation email to the team member's BambooHR profile in their `Contracts & Changes` folder. 
1. ***Specialist***: The Specialist will add the offboarding to the [People Exp/Ops Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e18b512#gid=488708224) in the `Specialist Offboardings` tab and add their name as a DRI, or ping the other Specialists to be certain that there is a fair balance and rotation of who is the DRI.
1. ***PBP***:The PBP will post a message in the `#offboardings` confidential Slack channel and provide the name, last day, reason for resignation, rehire eligibility and location of the individual. To share this information, the PBP will use the Slack shortcut icon and follow the steps below:

    * Click on the shortcut icon in the `#offboardings` slack channel 
    * Select the `Offboarding Form` workflow from the menu 
    * Submit the form and it will be posted to the `#offboardings` channel
    * Tag the People Experience team, IT Ops team, and Payroll directly after the form is posted to the `#offboardings` channel
    * _Note_: If a change needs to be made to the form only an Admin or Owner of Slack can make this change.<br><br>
    
1. ***Experience***: The People Experience Team will determine who will create the offboarding issue according to the [offboarding guidelines](/handbook/people-group/offboarding/offboarding_guidelines/), and confirm both in the `#offboardings` Slack channel and in the [People Exp/Ops Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e18b512#gid=0).
    
    * _Note: For resignations from Director-level and above, or for departures that have significant context pre-dating the exit interview that can help make the discussion more effective, the [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) will send out the exit survey and conduct the exit interview._     
    
1. ***Specialist***: The People Specialist will reach out to the team member directly to schedule an Exit Interview. 
      * The assigned People Specialist will first send out the Offboarding Informational Email according to the appropriate template. Current templates include [USA](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/offboarding_usa.md), [Canada](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/offboarding_canada.md) and [all other countries](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/offboarding_all_countries.md).
      * The assigned People Specialist will then send out the Exit Survey via Culture Amp (using the Exit Survey template in Culture Amp) and communicate with the departing team member to mention that they would like to schedule a non-mandatory exit interview, to occur during the team member's last week. 
      * To send the Exit Survey, log into Culture Amp and click on `Surveys` from the top menu bar. Select `Exit Survey` and select the `start` exit button - this will allow the People Specialist to assign the survey to the departing team member.
      * Remind the departing team member it is necessary to complete the [Exit Survey](#exit-survey) _prior_ to the Exit Interview so it can be discussed.
      * Once the exit interview has been completed and all notes entered, the assigned People Specialist will ping the PBP in the `#pbp-peopleops` Slack channel to confirm that the notes have been added and that the Exit Interview has been completed. This ping will occur within 48 hours of the exit interview.
1. ***Experience***: The People Experience team member will reach out to the departing team member directly to confirm the time at which they can proceed with the creation of the [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/offboarding.md) on the team member's last day.

#### Exit Survey

The Exit Survey provides team members with the opportunity to freely express views about working at GitLab.
People Operations Specialists will send the [CultureAmp](https://www.cultureamp.com/) survey to all team members who are leaving voluntarily. The People Specialist & the team member may proactively set up time to discuss their responses and ask for further information. Exit interviews are not mandatory, however your input will help provide GitLab with information regarding what is working well and what can be improved. Please point team member to the [Offboarding FAQ page](/handbook/people-group/offboarding/faq.html).

#### GitLab Alumni Program

All offboarding may request to be added to the Slack channel `gitlab-alumni` by requesting through the PBP who can review the request and confirm to the People Experience Associate who can open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) on behalf of the former team member.
Reasons that someone would not be permitted to join would be due to involuntary offboarding for extreme behavior or a violation of our [Code of Conduct](https://about.gitlab.com/handbook/people-group/code-of-conduct/).
Underperformance or other causes for involuntary offboarding are not a reason to exclude someone from the alumni program. 
The purpose of this channel is to network and socialize with team members.
Joining the channel is voluntary and subject to GitLab's [Code of Conduct](/community/contribute/code-of-conduct/).

GitLab, the company, monitors the channel and can remove people from it at their sole discretion.
The GitLab [code of conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) is enforced in the channel.

## Involuntary Offboarding

Involuntary offboarding of any team member is never easy. We've created some guidelines and information to make this process as humane as we can. Beyond the points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/underperformance), as well as the [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/offboarding.md).

If the need for Involuntary offboarding arises, the process is as indicate below:

#### Leave of Absence or Garden Leave

In some cases a team member will go on a Leave of Absence or Garden Leave prior to their actual offboarding date from GitLab.  Team members on LOA or Garden leave will have no access to GitLab systems and may not be required to do any work on GitLab's behalf. The People Experience team will generate the offboarding issue at the end of the team member's last working day, as per notification from the People Business Partner. Once the LOA or Garden leave expires the team member will be officially offboarded from GitLab.  Prior to the offboarding issue and the overall process for the term listed below.  The PBP will complete the following:

*  ***PBP***: PBP will have a legal/CPO review of the planned offboarding.
*  ***PBP***: PBP will inform payroll, compensation and benefits, security and the stock administration of the date the team member will have access suspended and the official offboarding date prior to the start of the official offboarding issue.  The PBP can either inform the group via the confidential #offboardings channel 1-2 days prior to the scheduled exit or via a zoom call.  
*  ***PBP***: PBP will work with the People Experience Team to ensure the offboarding issue has the correct dates and all GitLab offboarding team members in payroll, compensation and benefits, security and stock administration have been communicated to and understand the correct offboarding date. 

_Note: The offboarding process will remain the same as listed below in the `Involuntary Process` section below. The People Specialist will notify the team via the confidential #offboardings Slack channel to start to start turning off the team members access._ 

### Involuntary Process

The manager and the team member should have walked through the guidelines on [underperformance](/handbook/underperformance) before reaching this point.

1. ***Manager***: Reach out to the appropriate People Business Partner (PBP) for assistance.
    * PBP will ask about what the performance issues have been, how they have been attempted to be addressed, and review all manager/team member documentation.
    * Once the review has been completed and the decision has been made to offboard the team member, the PBP will partner with the People Specialists to coordinate the offboarding of the team member.
1. ***PBP***: The PBP will notify the People Operations Specialist team of the offboarding by posting in the `#pbp-peopleops` confidential Slack channel. The PBP will indicate the team member's name, last day, reason for resignation, and rehire eligibility. The first Specialist to respond to the post will be the Specialist that partners with the PBP for the offboarding process.<br>
_Note: If the team member has a contract with a co-employer, the payroll lead will forward the email to the contact at the co-employer._
1. ***PBP***: The PBP will create a private Slack channel that will include the PBP, Manager, Specialist, and Leader of the organization to review the offboarding and agreed upon offboarding date. 
1. ***Specialist***: If applicable, the People Specialist will prepare the severance agreement in preparation for the call between the PBP, Manager, and team member. Additional guidelines for the preparation of this agreement can be found below in the [Separation and Release of Claims Agreements](#separation-agreement) section. To determine whether or not a severance agreement is applicable, please refer to the `Severance Elgibility` guidelines accessible by PBPs and Specialists. 
   _Note: For any severance that has less than 48 hours turn around the People Business Partners will handle._
1. ***Manager***: Once the date and time is confirmed, the manager will schedule time with the team member and send the PBP a _private and separate calendar invite_ with the Zoom details for the meeting with the team member to share the news of the offboarding. 
1. ***Payroll***: If the team member is employed by a PEO/entity with statutory leave requirements, review if any time off needs to be paid on the last paycheck by looking in BambooHR in the `Time Off` section.
1. ***Specialist***: The Specialist will confirm and coordinate the date and time of the offboarding and who will be available to assist with the offboarding. At this stage, the name of the departing team member is ***not*** yet shared. To share this information, the Specialist will use the Slack shortcut icon and follow the steps below:

    * Click on the shortcut icon in the `#offboardings` slack channel ![Shortcut Icon](./Shortcuticonoffboarding.png)
    * Select the `InVol Hold Request` workflow from the menu ![Shortcut Menu](./Shortcutmenu.png )  
    * Submit the form and it will be posted to the `#offboardings` channel
    * Tag the People Experience team, IT Ops team, and Payroll directly after the form is posted to the `#offboardings` channel
    * _Note_: If a change needs to be made to the form only an Admin or Owner of Slack can make this change.<br><br>  

1. ***Specialist***: Will send a private calendar request to the identified People Experience, PBP, Security, and Payroll lead blocking time on calendar to indicate when the offboarding will begin. 
1. ***PBP/Manager***: Discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via video.
1. ***PBP/Manager***: Decide who will handle which part of the conversation, and if desired, practice it.
    * If needed the PBP will provide the manager with a script for the offboarding meeting. 
1. ***PBP/Manager***: If the team member who is being terminated is a people manager, a _communication plan for the team_ regarding the departure should be in place before the offboarding proceeds. The communication plan should include: identification of an interim leader, (ideally) a scheduled meeting with the interim leader for the direct reports, a scheduled team call to announce the current manager's departure and the interim leadership, and finally an announcement in the `#team-member-updates` Slack channel to share with the wider team. 
   * Important: 
      * Informing the team and answering questions should be the top priority.
      * No announcement should be made in the `#team-member-updates` Slack channel until the team call has been completed.
      * In most cases, a team call can occur the same day of the offboarding. If necessary, the offboarding can be announced in the `#team-member-updates` Slack channel the following day.
1. ***PBP/Manager***: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later. You can reference the [offboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/offboarding.md) for the full list of actions. This conversation should take place in the private Slack channel with PBP, Manager, Specialist, and Leader to provide visibility to the Specialist as well.
1. ***PBP***: If the team member is a risk to the production environment, the PBP should privately reach out to the `Infrastructure Managers` to determine who will be available to assist with the offboarding. Once an infrastructure team member has been identified, they should be added to the private calendar invite sent to People Experience, Security, People Specialist, and Payroll to hold the time for the team member offboarding. Once the offboarding conversation starts the PBP will privately Slack the infrastructure contact the name of the team member to start the offboarding process.

When on the call...

1. ***Manager***: Deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved. The Manager will make it clear that the decision is final, but will also explain what led to this decision and will point to the process that was followed to reach this decision. A sample leading sentence can be: 

_"Thanks for joining the call, [team member name].
Unfortunately, the reason I wanted to speak with you is because we have decided that we have to let you go and end your employment / contract with GitLab because of xyz."_

1. ***PBP***: As soon as the conversation begins, the PBP will ping the team members in the confidential `#offboardings` Slack channel with the name of the team member to immediately start the offboarding process.
1. ***Manager***: Hand the call over to the PBP to continue. 
1. ***PBP***: The PBP will also make it clear that the decision is final, but also will genuinely listen to the team member's side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. ***PBP***: Make sure to communicate the [practical points](#offboarding-points) from the offboarding memo outlined below.
1. ***Specialist***: Once the conversation is complete, the PBP will notify the Specialist to stage the severance document in HelloSign for review and signatures.
1. ***Specialists***: If appropriate (default is that it is appropriate), send an [offboarding memo](#sample-offboarding-memo) to the departing team member.

## Offboarding Slack Workflow 
This section will define key terms in offboarding slack workflow forms. 

### Offboarding Form 
- **Team Name**: Full Legal Name 
- **Job Title**: Fill in job title
   - Key Stakeholders: Security
- **Offboarding Effective Date**:	Termination Date for BHR (YYYY-MM-DD)
- **Garden Leave Effective Date**: The date a issue will be created and access will be turned off. The TM will stay on payroll through their offboarding effective date. (YYYY-MM-DD)
- **Issue Create Date and Time**:	When access should be turned off and offboarding processes will begin. (YYYY-MM-DD PST)
- **Type of Offboarding**:	Voluntary / Involuntary 
- **Offboarding Reason**:	Reasons available in BHR 
- **Eligible for Rehire, Alumni Channel**:	
   - Yes- Rehire, Yes- Alumni Channel
   - No- Rehire, No- Alumni Channel 
   - Yes- Rehire, No- Alumni Channel
   - No- Rehire, Yes- Alumni Channel
- **Team Member Location**:	Location
   - Key Stakeholder: Payroll 
-**Exit Interview Owner**: PBP / Specialist
   - Key Stakeholder: Specialists 

### Hold for In-Vol Offboarding
This form is used to help 
- **Date of Offboarding**
- **Time of Offboarding PST**  
- Location
- Anything else we should know 

## Critical Points During Offboarding Call
{: #offboarding-points}

The following points need to be covered for any team member:
1. Final Pay: "Your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “Please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “We know you are a professional, please keep in mind the agreement you signed when you were hired”.
1. If you would like GitLab to share your personal email with the rest of the company, please send an email to People Ops or a farewell message that can be forwarded on your behalf.

The following points need to be covered for US-based employees:
1. COBRA: “Your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (Lumity) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace.
If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from People Ops”.
1. Unemployment insurance: "It is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep GitLab informed: "If you move I want to be sure your W-2 gets to you at the end of the year.
You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason).

### Sample Offboarding Memo

If appropriate (to be determined by conversation with the manager, the Group Executive, and People Ops), use the following [offboarding memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation.
As written, it is applicable to US-based employees only.

### Separation and Release of Claims Agreements
{: #separation-agreement}

Separation and Release of Claims Agreements ***do not*** apply for all offboardings. To review in which cases they do/do not apply, please reference the `Severance Eligibility` document accessible by People Specialists and PBPs. In the case that a severance agreement is applicable, the steps below should be followed:

### Process for US-based Team Members

1. ***Specialist***: The People Specialist assigned to the particular offboarding case at hand should select the appropriate severance template. Options include: Non-California over 40, California over 40, Non-California under 40, California under 40. 
1. ***Specialist***: The People Specialist should make a copy of the template and save it in the `Copies of Individual Severance Agreements` folder. 
1. ***Specialist***: The People Specialist should fill out the document and share with the PBP and Legal for final review/approval. 
1. ***Specialist***: The People Specialist will stage the document in HelloSign for signatures. Please note that depending on the template used, team members have a limited amount of time to sign the Separation and Release of Claims Agreement.


*  When staging the document for signatures, please note:
    *  Remember to send the document to the team member's _personal_ email address
    *  Select the `assign signature order` option in HelloSign to ensure the team member signs the document first
    
1. ***Specialist***: When the signed document is received, the Specialist should upload it to the team member's BambooHR profile in the `termination` folder
1. ***Specialist***: As a final step, the Specialist should ping the `#payoll_peopleops_terms` private channel and `@menion` the appropriate PBP to confirm that the Severance document has been signed and uploaded to BambooHR

_Important Notes:_
* Separation pay is not paid until the ex-team member signs the document and the revocation period has passed.
* Make sure you understand the rules for over 40.
* You must use language in your exit meeting that is in no way forceful of the ex-team member to sign the agreement. Use phrasing such as “if you choose to sign”; “you have a right to have legal council review this document before you sign”, etc.


#### Process for Team Members Outside of the US

_To be documented_ 

## Communicating Departures Company-Wide

As explained briefly in the [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/offboarding.md), GitLab does not provide any context around why people are leaving when they do.
However as mentioned in the procedures, for [voluntary offboarding](/handbook/people-group/offboarding/#voluntary-offboarding), the team member can share their reasons for leaving if they wish.

If they choose not to then the departing team member's manager shares the following in the `#team-member-updates` Slack channel:  

> "As of today, [X] ([X's job title] from the [X's team] team) is no longer with GitLab. I would like to take this opportunity to thank X for their contributions and wish them all the best for the future.
> If you have questions about tasks or projects that need to be picked up, please let me know.
> If you have concerns, please bring them up with your manager."

If someone is let go involuntarily, this generally cannot be shared since it affects the individual's privacy and job performance is intentionally kept [between an
individual and their manager](/handbook/communication/#not-public).
Unless it's for extreme behaviour/gross misconduct, the team member will be advised the areas of concern about their performance, given support and guidance on how to improve and provided with an appropriate timeframe to reach and sustain the expectations.

If you are not close to an employee's offboarding, it may seem unnecessarily swift.
Please remember that these decisions are never made without following the above process to come to a positive resolution first - we need to protect the interests of the individual as well as the company, and offboarding are a last resort.
According to our values [negative feedback is 1-1 between you and your manager](/handbook/values/#negative-feedback-is-1-1) and we are limited in what we can share about private employee issues.
Please discuss any concerns you have about another employee's offboarding with your manager or your People Business Partner.

Given the expectations and responsibility that come with a VP and above position, when there is an involuntary offboarding for one of these positions, additional context for the personnel change can be provided to the organization.  

Silence is unpleasant. It's unpleasant because we are human, which means that we are generally curious and genuinely interested in the well-being of our team members.

Is it _more_ unpleasant in a remote setting? Probably not.
When people are all in the same office building, they can "kinda sorta" know what may be coming because of the grapevine, the water cooler, and so on. When the news hits it might be less of a shock - only because of unprofessional behavior in the first place.
But at larger companies with multiple office buildings, departures will tend to come as more of a surprise and with less context (at least to the people in other buildings).

## What Do We Share?

We strive to maintain personal information regarding all team members private, this includes information regarding a team members voluntary or involuntary departure from GitLab.
However, a manager with the consent and approval of the departing team member can share more details with the GitLab team regarding the decision to leave GitLab.

For a voluntary departure a team member may have chosen to leave for many different reasons, career development, promotion, a new role or career path, dislike remote work, etc.
For example, a team member may tell their manager that they really miss being in an office environment and remote work is not suitable for their personality.
Based on that decision they have taken another opportunity that allows them to go to an office.

If the departing team member gives their manager permission to share that information then the manager will share while making the departure announcement on the team call.
We want all GitLab team-members to be engaged, happy and fulfilled in their work and if remote work, the requirements of the job or the role it self are not fulfilling we wish our team members the best of luck in their next endeavor.

Regarding involuntary offboarding, certain information can also be shared with the GitLab team regarding the departure.
Some team members do not thrive or enjoy the work that they were hired to do.
For example after starting at GitLab a team member quickly learns they have no desire or interest to learn Git or work with Git.  This doesn't make them a bad person, it just means they don't have the interest for the role and based on that the decision was made to exit the company.
Again, we want all team members to enjoy and thrive in their work and that may or may not be at GitLab.

The departing team member may author a goodbye message for either voluntary or involuntary offboarding:
  1. Write your message, for example, "It was a pleasure working with you all.
  I've decided to pursue an opportunity in a different industry that is more aligned with my interests.
  I'll be rooting for GitLab from the sidelines.
  Stay in touch: Cheers!"
  1. Send it to your manager and People Partner for approval.
  1. The manager can with the permission of the team member post the agreed upon message verbatim as long as the message is deemed appropriate in the company call agenda and slack goodbye announcements.
  1. If appropriate, managers are encouraged to thank departing team members for their contributions and wish them the best of luck on their future endeavors.

In some instances there will be no further clarification on why a team member has departed, if there are concerns you can address those with your manager.
Different levels of transparency will exist based on maintaining respectful treatment for all departures.
Having team members leave may be a learning opportunity for some, but should not be a point of gossip for anyone.
Managers will need to balance the opportunity for learning with the expectation of privacy and consult their People Business Partner should they have questions.

Transparency is one of our values.
In the case of offboarding transparency [can be painfully specific, calling out an employee’s flaws, while inviting more questions and gossip](https://outline.com/PTGkER).
We opt to share the feedback only with peers and reports of the person since we balance transparency with our value of collaboration and negative is 1-1.

## Turnover Data

GitLab's [turnover data](https://docs.google.com/a/gitlab.com/spreadsheets/d/1yk_tlu-Qy3j4VbaB8Yt4wMc5Gocxri9cp7F57DVWyYM/edit?usp=sharing) is only viewable internally.
This data is updated on a monthly basis by People Operations.

### Managing the Offboarding Tasks

#### Offboarding Issue
To track all tool deprovisioning, please open an offboarding issue following the [offboarding guidelines](/handbook/people-group/offboarding/offboarding_guidelines/).

#### Returning Property to GitLab

As part of offboarding, any GitLab property valued above 1,000 USD needs to be returned to GitLab. 

For laptops, please refer to the [Laptop Buy Back Policy](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptop-buy-back-policy) which states that team-members have the option to buy back their existing laptops either when it gets refreshed for a new one, or when the team member is offboarding. If the team member has completed 1 calendar year or more at GitLab at the time of laptop refresh or offboarding, they can opt to keep their laptop at no cost. If the team member hasn't completed 1 calendar year at GitLab at that time, they have the option to purchase their laptop for current market value.

To return your laptop to GitLab, please contact itops@gitlab.com immediately upon offboarding.

#### Expensify
This section of the Accounting Department.

To remove someone from Expensify Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar.
Select the right policy based upon the entity that employs the team member. Select "People" in the left menu.
Select the individual's name and click "Remove".
If the person has a company credit card assigned to them please notify Finance before un-assigning it.

### Evaluation

1. How could this outcome have been avoided?
2. Were there early signs that were missed?
3. In retrospect, what questions should have been asked to bring awareness and
  ownership to performance issues?
For example, "How would you compare yourself relative to your peers?"
People are surprisingly honest here.

### Retrospective for Managers
For involuntary offboardings it is optional to do a retrospective on the hiring, onboarding and coaching/communication of the departing team member. As a manager, you can use [this template](https://docs.google.com/document/d/1AWth5o_sagDTwQ92FJr7WDHPNezEg6-BgRY6Tafejgo/edit?usp=sharing) for a retrospective. Please share the filled out template with your manager as well as the [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) for your group.

Within the Engineering division this is a **required process** because it causes hiring managers to reflect on what led to the ultimate decision of parting ways with the team member, and how that might be prevented during future hiring processes.

## Declination (Prior to Day 1 and up to Day 5)

Should a new hire be onboarded by the People Experience team, but: 

* Communicate very close to their start date that they have declined to continue, or
* Decline on their first day, or within their first 5 days, then

This is not seen as a true offboarding, but there are still relevant steps to take.

1. The People Experience Associate will manually create a skeletal [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/offboarding.md) and only include deprovisioning tasks that are applicable to the access that was given up to that point.
1. The People Experience Associate will inform the [team member with ownership rights](https://about.gitlab.com/job-families/people-ops/people-operations/#director-global-people-operations) of BambooHR to delete the team members' BambooHR profile entirely by messaging them in the private People Ops Slack channel.

## PEO Offboarding Guidelines

As part of the [country conversion process](/handbook/people-group/contracts-and-international-expansion/#country-conversions), the People Specialist team obtains information from our PEOs in each country regarding offboarding processes and requirements. All information obtained is documented below:

