---
layout: handbook-page-toc
title: "Code of Business Conduct & Ethics"
description: "Overview of the code of conduct and ethics at GitLab."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Code of Business Conduct & Ethics

GitLab is committed to serving our customers and employing individuals with personal standards consistent with that of our [values](/handbook/values/). This Code is designed to deter wrongdoing and to promote:

- Honest and ethical conduct, including the ethical handling of actual or apparent conflicts of interest between personal and professional relationships.
- Full, fair, accurate, timely, and understandable disclosure in reports and documents we file with regulatory agencies and in our other public communications.
- Compliance with applicable laws, rules, and regulations.
- The prompt internal reporting of violations of this Code.
- Accountability for adherence to this Code.

Our Code applies to all directors, officers, employees, and contractors of GitLab and its affiliates and subsidiaries. Agents and vendors of GitLab are also expected to read, understand, and abide by this Code.

This Code should help guide your conduct in the course of our business. Many of the principles described in this Code are general in nature, and the Code does not cover every situation that may arise. Use common sense and good judgment in applying this Code. If you have any questions about applying the Code, please seek guidance. Not all information regarding the conduct of our business is found in this Code. Please review the applicable policies and procedures in specific areas as they apply as found in our [Team Handbook](/handbook/).

The Code of Conduct will be reviewed on an annual basis to ensure compliance with applicable laws and industry standards.

### Complying with the Code

To maintain the highest standards of integrity, we must dedicate ourselves to complying with this Code, company policies and procedures, and applicable laws and regulations. Violations of this Code not only damage our company’s standing in the communities we serve, they may also be illegal. Team members involved in violating this Code will likely face negative consequences. GitLab will take the appropriate disciplinary action in response to each case, up to and including termination. In addition, team members involved may be subject to government fines, or criminal or civil liability.

### Reporting Violations

If you think this Code or any GitLab policy is being violated, or if you have an ethics question, you have several options:

- Discuss the issue with your supervisor or manager.
- Discuss the issue with another supervisor or manager.
- Contact the [People Group](/handbook/people-group/)
- Contact [GitLab’s 24-hour hotline](#how-to-contact-gitlabs-24-hour-hotline).

All reports (formal or informal) made to a GitLab supervisor, manager or executive should be promptly escalated to the People Business Partners or to the Chief People Officer (CPO). GitLab will then review the report promptly and thoroughly to determine if an investigation is warranted.

#### Investigation Process

If the People Group has determined it appropriate, GitLab will promptly initiate an appropriate investigation into all possible violations of law and/or GitLab policy. The People Business Partner assigned to the business department will investigate all reports unless:

- the complaint is against a member of the People Business Partner team, in which case the investigation will be conducted by Legal (internal Chief Legal Officer or outside counsel, as appropriate).
- if the case requires the People Business Partner to investigate in their own Client group, the CPO and the People Business Partner will determine if a People Business Partner not supporting the group should lead the investigation.
- the complaint is made against a member of the executive team or there are multiple complainants regarding the same individual and/or issue, then outside counsel will be retained by the CPO to conduct the investigation. The Board of Directors will be notified of any complaints made against a member of the executive team alleging illegal conduct and/or egregious unethical conduct.

If the complaint involves a member of the executive team that has direct oversight for the Legal department or involves a member of the Legal department, then the CPO will work with outside counsel to conduct the investigation. If the complaint involves a member of the executive team that has direct oversight for the People Group or involves a member of the People Group, then Legal will work with outside counsel to conduct the investigation.

GitLab expects all employees and contractors to cooperate fully and candidly in investigations.

#### Investigation Timeline

GitLab will make all reasonable efforts to initiate an investigation into the allegation(s) and conclude the investigation in a timely fashion. Depending on the type of investigation the steps and timeline for each investigation will vary.

#### Investigation Findings

The investigation findings will be reported back to the CPO. Based on the investigation findings, the CPO will make a determination as to whether the allegation(s) were founded, unfounded or inconclusive. This determination will be documented in writing and made part of the investigation report. The determinations are as follows:

- **Violation Found**: Where a violation of GitLab policies, workplace rules or law is found to have occurred, the CPO will review the findings and make a recommendation for corrective action to the executive leader of the accused's reporting line. Together, the CPO and the business unit will determine the proper corrective action. If the accused is a member of the executive team then the CPO will confer with the CEO, and where necessary, the Board of Directors. Once a corrective action has been determined, the accused will be notified of the finding and of the specific corrective actions to be taken. The accused employee's manager will also be notified if appropriate. No details about the nature or extent of disciplinary or corrective actions will be disclosed to the complainant(s) or witness(es) unless there is a compelling reason to do so (for example, personal safety).
- **No Violation Found**: In this situation, the complainant (if known) and the accused should be notified that GitLab investigated the allegation(s) and found that the evidence did not support the claim.
- **Inconclusive investigation**: In some cases, the evidence may not conclusively indicate whether the allegation(s) were founded or unfounded. If such a situation occurs, the complainant (if known) and the accused will be notified that a thorough investigation was conducted, but GitLab was unable to establish the truth or falsity of the allegation(s). GitLab will take appropriate steps to ensure that the persons involved understand the requirements of GitLab's policies and applicable law, and that GitLab will monitor the situation to ensure compliance in the future.

#### How to Contact GitLab's 24-hour hotline:

GitLab has engaged Lighthouse Services to provide an anonymous ethics and compliance hotline for all team members. The purpose of the service is to insure that any team member wishing to submit a report anonymously can do so without the fear of [retribution](/handbook/people-group/people-policy-directory/#commitment-to-non-retaliation).

Reports may cover but are not limited to the following topics: ethical violations, wrongful discharge, unsafe working conditions, internal controls, quality of service, vandalism and sabotage, [sexual harassment](/handbook/anti-harassment/#sexual-harassment), theft, discrimination, conduct violations, alcohol and substance abuse, threats, fraud, bribery and kickbacks, conflict of interest, improper conduct, theft and embezzlement, violation of company policy, violation of the law, misuse of company property, falsification of contract, reports or records.

Please note that the information provided by you may be the basis for an internal and/or external investigation into the issue you are reporting and your anonymity will be protected by Lighthouse to the extent possible by law. However, your identity may become known during the course of the investigation because of the information you have provided. Reports are submitted by Lighthouse to a company designee for investigation according to our company policies.

Lighthouse Services toll free number and other methods of reporting are available 24 hours a day, 7 days a week for use by team members.

- Website: [https://www.lighthouse-services.com/gitlab](https://www.lighthouse-services.com/gitlab)
- USA Telephone:
    - English speaking USA and Canada: 833-480-0010
    - Spanish speaking USA and Canada: 800-216-1288
    - French speaking Canada: 855-725-0002
    - Spanish speaking Mexico: 01-800-681-5340
- All other countries telephone: +1-800-603-2869
- E-mail: reports@lighthouse-services.com (must include company name with report)
- Fax: (215) 689-3885 (must include company name with report)

The reports sent to Lighthouse Services are shared with the [Chief Legal Officer](/job-families/legal/chief-legal-officer/) and [the Chair of the Audit Committee](/handbook/board-meetings/#audit-committee).

### Material Nonpublic Information / Inside Information

Employees know that they have to protect Confidential Information. But did you know that employees also cannot **use** Confidential Information? Employees are not permitted to use or share Confidential Information for purposes of trading securities, illegal activities or for any other purpose except to conduct [GitLab](/handbook/people-group/acceptable-use-policy/) business. Insider trading (or dealing) laws and regulations globally prohibit buying or selling a company’s securities while in possession of Confidential Information about that company that is Nonpublic and Material.

Employees can also violate these laws by disclosing Material Nonpublic Confidential Information to another person if, as a result, that person – or any other person – buys or sells a security while aware of that information. If you make such a disclosure or use such information, there can be stiff penalties, even if you yourself stand to make no financial gain.

Clarity on whether information is “Nonpublic” or “Material” or what restrictions exist on the use or distribution of such information is provided below. Further questions should be directed to the Legal or Compliance Department.

**Definitions**
**Nonpublic Information** is Confidential Information that is maintained or otherwise handled by GitLab that may be harmful to GitLab, it’s employees or others with whom GitLab does business as defined in the Data Classification policy, including, but not limited to, any of the following:
Information used for, or obtained from, a rated entity or its agent for the purpose of determining a rating or pending rating;
Major personnel changes;
Lawsuits that may trigger heavy damages;
Mergers, acquisitions;
Substantial contracts not in the ordinary course of business;
Information concerning the rating committee process, including, but not limited to, the voting breakdown in the committee, the fact that a member of the rating committee disagreed with the ultimate committee decision, and the names or titles of members of the committee;
Nonpublic Information relating to GitLab’s customers and information provided by GitLab’s customers, including internal risk assessment models, information concerning the performance of those models, historical default data for loan portfolios, customer-specific loan pricing information and information concerning portfolio composition and concentration;
GitLab’s nonpublic financial information and sales projections; or
Information regarding GitLab’s business plans, strategies, proprietary systems, algorithms, formulas, methodologies, product designs, processes, research and development information and trade secrets; or Special Personal Information.

**Material Information** has no precise definition and is subject to a variety of interpretations. Accordingly, for the purposes of this Policy, “Material Information” refers to any information that: (i) might have an effect on the market for a security generally; or (ii) might affect an investment decision of a reasonable investor.

Examples of Material Information may include, but are not limited to:
sales results
earnings or estimates (including reaffirmations or changes to previously released earnings information)
dividend actions
strategic plans
new products, discoveries or services
major personnel changes
Merger, acquisition and divestiture plans
Financing plans
Proposed securities offerings
Government actions
Substance of major litigation or potential claims
Negotiation or termination of major contracts
Rating or pending rating actions

If you are not sure whether or not a particular piece of information is Material Information, you should err on the side of caution and assume that it is Material Information. In other jurisdictions, Material Information may be referred to as “inside information” or “price-sensitive information”.

Material Nonpublic Information refers to information that is both Material and Non-Public Confidential Information.

### GitLab Internal Acceptable Use Policy

The [Gitlab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/) specifies requirements related to the use of GitLab computing resources and data assets by GitLab team-members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. Our intention in publishing this [policy](/handbook/people-group/acceptable-use-policy/) is not to impose restrictions but outline information security guidelines intended to protect GitLab assets.

### Protecting Customer/Third Party Information Privacy

We take the protection of privacy for our customer’s, consumer’s, and other third parties that have entrusted us with information very seriously. Customer or third party information includes any information about a specific customer/third party, including such things as name, address, phone numbers, financial information, etc. Specifically, note that:

- We follow all applicable laws and regulations directed toward privacy and information security. Keeping customer information secure and using it appropriately is a top priority for our company.
- We must safeguard any confidential information customers or third parties share with us.
- We must also ensure that such information is used only for the reasons for which the information was gathered, unless further use is allowed by law.
- We do not disclose any information about a third party without their written approval unless legally required to do so (for example, under a court-issued subpoena).

If you do not have a business reason to access this information, you should not do so. If you do, you must also take steps to protect the information against unauthorized use or release in line with our [Security Best Practices](/handbook/security/).

### Intellectual Property and Protecting IP

Our [intellectual property](/handbook/contracts/#piaa-agreements) is among our most valuable assets. Intellectual property refers to creations of the human mind that are protected by various national laws and international treaties. Intellectual property includes copyrights, patents, trademarks, trade secrets, design rights, logos, expertise, and other intangible industrial or commercial property. We must protect and, when appropriate, enforce our intellectual property rights. We also respect the intellectual property belonging to third parties. It is our policy to not knowingly infringe upon the intellectual property rights of others.

1. Take proper care of any **confidential** information you get from our customers.
1. As an employee or contractor, the things you create for GitLab belong to the company.
    - This work product includes inventions, discoveries, ideas, improvements, software programs, artwork, and works of authorship. This work product is the company’s property (it does not belong to individuals) if it is created or developed, in whole or in part, on company time, as part of your duties or through the use of company resources or information.
1. If you copy code always **check** the license and attribute when needed or appropriate.
1. Check **community contributions** and do not merge it when there can be doubt about the ownership.
1. Only certain authorized individuals may **sign** legal documents such as NDAs. Please refer to our [signature policy](/handbook/finance/authorization-matrix/)
1. View our [DMCA policy](/handbook/dmca) in regards to copyright / intellectual property violations

Assignment of intellectual property is addressed in the [employee and contractor templates](/handbook/contracts/#employee-contractor-agreements), but these may vary from what you agreed to at the time of your contract. For specific information about your obligations regarding intellectual property rights and obligations, please reference your contract.

### Antitrust and Fair Competition

All directors, officers, employees, and contractors must comply with antitrust and competition laws which prohibit collusive or unfair business behavior that restricts free competition. These laws are quite complicated, and failure to adhere to these laws could result in significant penalties imposed on both GitLab and the employees and/or contractors who violated the law.

Unlawful behavior examples:

- entering into agreements with competitors to fix prices, terms of sale or production output
- to engage in bid rigging
- to divide markets or customers
- to attempt to discriminate in prices or terms of sale among our customers
- to otherwise restrict the freedom of our customers to compete
- refusing to deal with certain customers or competitors

Such laws prohibit efforts and actions to restrain or limit competition between companies that otherwise would be competing for business in the marketplace. You must be particularly careful when you interact with any employees, contractors or representatives of GitLab’s competitors, especially at trade association meetings or other industry or trade events where competitors may interact. Under no circumstances should you discuss customers, prospects, pricing, or other business terms with any employees or contractors or representatives of our competitors.

If you are not careful, you could find that you have violated antitrust and competition laws if you discuss or make an agreement with a competitor regarding:

- Prices or pricing strategy
- Discounts
- Terms of our customer relationships
- Sales policies
- Marketing plans
- Customer selection
- Allocating customers or market areas
- Contract terms and contracting strategies

Depending on business justification and effect on competition, other practices not involving competitors may also result in civil violations of the antitrust and competition laws. These practices include:

- Exclusive dealing
- Bundling / package offerings
- Resale restrictions
- Selective discounting

We engage in open and fair procurement activities regardless of nationality or the size of the transaction. Suppliers are selected on a competitive basis based on total value, which includes quality, suitability, performance, service, technology, and price. We strive toward establishing mutually beneficial relationships with our suppliers based on close cooperation and open communication. Terms and conditions defining our relationship with suppliers are communicated early in the supplier selection process. Any agreements to such terms and conditions, or any acceptable modifications, are reached before work begins.

### Obtain Competitive Information Fairly

Gathering information about our competitors, often called competitive intelligence, is a legitimate business practice. Doing so helps us stay competitive in the marketplace; however, we must never use any illegal or unethical means to get information about other companies.

Legitimate sources of competitive information include:

- publicly available information such as news accounts
- industry surveys
- competitors' displays at conferences and trade shows
- information publicly available on the Internet
- from customers and suppliers (unless they are prohibited from sharing the information)
- by obtaining a license to use the information or actually purchasing the ownership of the information

When working with consultants, vendors, and other partners, ensure that they understand and follow GitLab policy on gathering competitive information.

### Anti-Money Laundering

Money laundering is a global problem with far-reaching and serious consequences. Money laundering is defined as the process of converting illegal proceeds so that funds are made to appear legitimate, and it is not limited to cash transactions.

Complex commercial transactions may hide financing for criminal activity such as terrorism, illegal narcotics trade, bribery, and fraud. Involvement in such activities undermines our integrity, damages our reputation and can expose GitLab and individuals to severe sanctions.

Our company forbids knowingly engaging in transactions that facilitate money laundering or result in unlawful diversion. Anti-money laundering laws require transparency of payments and the identity of all parties to transactions. We are committed to full compliance with anti-money laundering laws throughout the world and will conduct business only with reputable customers involved in legitimate business activities and transactions.

### Selection and Use of Third Parties/Procurement (Fair Purchasing)

We believe in doing business with third parties that embrace and demonstrate high principles of ethical business behavior. We rely on suppliers, contractors, and consultants to help us accomplish our goals. They are part of the GitLab team and should be treated according to our values. To create an environment where our suppliers and consultants have an incentive to work with GitLab, they must be confident that they will be treated in an ethical manner. We offer fair opportunities for prospective third parties to compete for our business. The manner in which we select our suppliers and the character of the suppliers we select reflect on the way we conduct business.

### Anti-corruption / Anti-bribery

Globally, many countries have laws that prohibit bribery, kickbacks, and other improper payments. No GitLab employee, contractor, officer, agent, or vendor acting on our behalf may offer or provide bribes or other improper benefits in order to obtain business or an unfair advantage. You must avoid participating in commercial bribery and kickbacks, or even the appearance of it, in all of our business dealings. Even in locations where such activity may not technically be illegal, it is absolutely prohibited by our company policy.

**Definitions**

1. Commercial bribery involves a situation where something of value is given to a current or prospective business partner with the intent to obtain business or influence a business decision.
1. Kickbacks are agreements to return a sum of money to another party in exchange for making or arranging a business transaction.
1. A bribe is defined as directly or indirectly offering "anything of value" to influence or induce action, or to secure an improper advantage.
1. "Anything of value" is very broadly defined and can include such things as:
    - Cash
    - Gifts
    - Meals
    - Entertainment
    - Travel and lodging
    - Personal services
    - Charitable donations
    - Business opportunities
    - Favors
    - Offers of employment

**Situations**

1. No employee or contractor shall make or promise to make, directly or indirectly, any payment of money or object of value to any foreign official of a government, political party, or a candidate for political office for the purpose of inducing or influencing actions in any way to assist our company in obtaining or retaining business for or with GitLab.
1. The exchange of appropriate gifts and entertainment is often a way to build our business relationships. However, you must conduct business with customers, suppliers, and government agencies (including U.S. and non-U.S. governments) without giving or accepting bribes including (but not limited to) commercial bribery and kickbacks.

### Gifts and Entertainment

Modest gifts, favors, and entertainment are often used to strengthen business relationships. However, no gift, favor, or entertainment should be accepted or given if it obligates, or appears to obligate, the recipient, or if it might be perceived as an attempt to influence fair judgment.

In general, unless you have supervisory approval you should not provide any gift or entertainment to customers, suppliers, or others that you would not be able to accept from a customer, supplier, or other applicable parties.

All directors, executives, and anyone else in the company participating in vendor selection, must disclose all gifts and entertainment valuing over US$250 for the six months prior to the vendor selection and during the term of the services and for a period of twelve months after services have been completed. The disclosure shall be made to the Legal department, and shall include the value of the gift or entertainment, the individual or company providing the gift, favor, or entertainment, and the date on which it was received. If you have any questions relating to this section, feel free to contact the Legal department.

### Trade Compliance (Export/Import Control)

GitLab is not to engage in activities that could threaten foreign policy and national security interests of the United States or other countries in which GitLab has facilities, international peace and security by distribution of products to end-users that could violate applicable export control laws. This Trade Compliance Policy defines the security transaction controls and control procedures to be developed and implemented at GitLab.

**What?**

_An export is the transfer of goods, technology, technical data and software transfer to a foreign country or a foreign national. Be advised that “export” includes downloads, electronic access to technology, emails and even visual reviews._

**Goods, Technology, Technical Data & Software Transfers**

Goods, technology, technical data & software transfers may be subject to Export Administration Regulations (the “EAR”) and also to the export control laws of other countries in which GitLab operates. For example, exports by GitLab from Canada would be subject to the EAR and also to Canada’s Export and Import Permits Act and the Canadian Export Control List (the “ECL”). For export purposes, technology and technical data includes any information that can be used or adapted for use in the design, development, manufacture, utilization, or reconstruction of products or commodities. This can include more than tangible items. For example, intangible items such as technical services, presentations, product demonstrations and software would be included in technology and technical information for the purposes of export control laws.

For the purposes of the EAR, an export of technology, technical data, and/or software is defined as the release of technology or software subject to the EAR, released in a foreign country and/or any release of technology or source codes (does not include object codes) subject to the EAR to a foreign national. Release to foreign nationals is deemed to be an export to the home country or countries of the foreign national (a “Deemed Export”). Some other countries, such as Canada, do not control Deemed Exports under their export control laws, however, US export control laws will still apply to Deemed Exports within those countries.

A Deemed Export does not apply to persons lawfully admitted for permanent residence in the United States (“green card” holders) and does not apply to persons who are protected under the Immigration and Naturalization Act.

It is possible that a license may have to be obtained prior to visits with foreign nationals or discussions of technical information with foreign nations, whether these occur at US facilities or in other countries.

**Release for Export includes:**

> Visual inspection by foreign nationals of U.S. equipment and facilities such as demonstrations and on-site training;
> Oral and written exchanges of information in the U.S. or abroad. This includes exchange or transmission of information through any media such as facsimile, email, internet, intranet, telephone, downloading, or causing the downloading, of software to locations outside the U.S. or to foreign nationals.
> Applications of situations abroad of personal knowledge or technical experience acquired in the U.S.; or
> Downloading or actual physical shipment of the technology or software.
> Similar rules apply in other countries in respect to what is or is not considered to be an export.

**NOT subject to Export Administration Regulation (EAR) Control**
The following technology, technical data and software are not subject to and are outside the scope of the EAR. However, other policy controls and restrictions may apply:

> Technology, technical data and software that is publicly available, meaning published in periodicals, books, print, or electronic media that is available to the public at a price that does not exceed the cost of reproduction or distribution;
> Data that is readily available at universities or other public libraries;
> All patent information available at any patent office;
> All technical data presented at open conferences, meetings, seminars or trade shows, provided the gathering is open to all technically qualified members of the public and that attendees are permitted to take notes or otherwise make a record of the proceedings;
> Technology, technical data or software arising during, or resulting from, fundamental research (as described in the EAR); or
> Technology, technical data or software that is educational in nature (as described in the EAR).

Other countries have similar exemptions, however, the specific exemptions for each country should be reviewed if technology, technical data or software is being exported from those countries to make sure that the exemptions recognized under U.S. law are also recognized under the law of the applicable country from which an export is being considered.

Release of information into the public domain is a conscious decision on the part of GitLab. In many cases, technology (in the form of product data sheets or technical specifications) has been released through distribution at public trade shows. Technology in the form of detailed drawings for production or manufacturing procedures is not public domain data.

**How?**

_Items are given special classification numbers (ECCNs). These numbers help determine whether a license is required. To determine an ECCN, you must know the ECCNs of the technology that was included in the overall export. Items that are EAR99 are the easiest to export. That said, the addition of a highly-regulated piece of technology can change the overall ECCN of the entire product. Other countries have their own classification systems that specify what is and is not controlled under their export control laws. When obtaining new good, software or technology from third party – make sure you know the ECCN or if an ECCN is not available, the applicable foreign law classification for the applicable good, software or technology._

**Company Type Classification**
GitLab’s current product Export Control Classification Number is 5D992.c. (Contact the Legal Department for a copy of letter.)

**New Projects/Products/Tenders**
The relevant business unit will work with The Legal Department to determine the export classification of new software as soon as possible during the project. Please note that changes to the encryption functionality may require a new export classification. This will usually occur after feature scope is determined and the origin of technologies to be used in the project is known. If an applicable product is going to be exported from GitLab’s facilities outside of the U.S., then an export classification will also need to be done for that country.

**Ongoing Surveillance of Export Classification**
All Product Classification sheets will be reviewed annually to ensure the export classification of the software has not changed, if GitLab is still exporting that software. GitLab will also monitor regulation changes that might affect the classification of GitLab products or other items that could be exported.

**Standard Terms and Conditions of Sale**
Our standard terms and conditions of sale contain text substantially similar to the following:
*Exports and U.S. Government Rights. The Products furnished to you may be subject to export and other restrictions under the laws and regulations of the United States of America and other countries. You hereby agree that you shall not transfer, export or re-export, directly or indirectly, any Product or technical data received from GitLab to any destination or entity subject to export or other restrictions under the laws and regulations of the United States of America or any other applicable countries unless prior written authorization is obtained from GitLab and the appropriate United States and applicable foreign agencies. The Products are provided with Restricted Rights. Use, duplication or disclosure by the U.S. government is subject to restrictions as set forth in (a) this Agreement pursuant to DFARs 227.7202-3(a); (b) subparagraph (c)(1)(i) of the Rights in Technical Data and Computer Software clause at DFARs 252.227-7013; or (c) the Commercial Computer Software Restricted Rights clause at FAR 52.227-110 subdivision (c)(1) and (2), as applicable. *

**Where?**

_Some countries (and nationals of those countries) are prohibited from accessing US Technology._

_Countries prohibited from receiving any technology include: Cuba, Iran, North Korea, Sudan, Syria and the Crimean Region of Ukraine._

_US Government Classified Information must only be accessible by US Citizens who have appropriate clearance and a need to know. If you know or believe GitLab is in receipt of US or Foreign Government Classified Information, please contact Legal or Compliance._

_Additionally, companies cannot agree to boycott any other country. If you receive any request to boycott a country, please notify Legal._

**Embargoed Countries**
The United States restricts imports and exports to certain destination without a specific authorization from the government. Products exported to an entity within an embargoed country or to a foreign national of that country, e.g., **Cuba, Iran, North Korea, Sudan, Syria, and the Crimean Region of Ukraine** will require a license, even if the shipment is through a reseller or via another country. Some countries have legislation that prohibits the imposition of U.S. embargo requirements on individuals and companies in those countries (“Blocking Legislation”).

**U.S. Anti-Boycott**
U.S. Anti-Boycott regulations exist to counteract foreign economic boycotts that are at odds with U.S. policy. It is GitLab's policy to comply with the requirements of Anti-Boycott regulations. Actions prohibited by the regulations include:

> Refusing (or agreeing to refuse) to do business with entities prohibited under a foreign boycott.
> Discriminating against a person based on race, religion, sex, national origin, or nationality condemned by a foreign boycott.
> Furnishing information that would assist in furthering a foreign boycott.
> Implementing letters of credit that include prohibited boycott terms or conditions.

Employees encountering requests or issues to avoid certain countries, nationals or protected individuals should contact Legal immediately. Under certain circumstances, the anti-boycott laws may require filing a report with the U.S. Department of Commerce regarding the request to participate in the boycott even if the employee correctly refuses to participate. Other countries have similar anti-boycott provisions.

**Who?**

_Some people have been prohibited from receiving technology by various government agencies. To ensure that a particular person or entity is not prohibited, a Denied Party Screen must be completed before any export occurs. Companies with whom GitLab does business, must undergo a due diligence review which includes a Denied Party Screen, valid contract and background check of the entity._

**Due Diligence**
If there are any suspicions about any particular customer, reseller, or order, they should be discussed with the Export Compliance Officer or Legal.

There must be sufficient information provided about a customer to ensure a proper Denied Party Screen can be completed. If this information is missing, the Sales person shall contact the reseller or the end-user to ask them to provide the requisite information.

To ensure traceability the individual performing the Denied Party Screen shall record the end-user site details for all approved orders received directly from end-users as well as any resellers. It is important to always be assured that the end-user and the end-use is legitimate.

**Denied Party Screens**
_Prior to the release of technology to a foreign country or foreign national_, sales operations, order processing, shipping, engineering and/or licensing shall ensure that the receiving person or entity is not on an Export Control Watch List by conducting a Denied Party Screen. If an export is being done from a country other than the U.S., then the denied parties list for that country will need to be checked in addition to the US denied persons screening. If any of the customers are on any of the Export Control Watch Lists, the Transaction will be escalated to the Export Control Officer who will work with the relevant departments and government agencies to determine if the delivery is allowed. It is important to note that products that are being shipped to an entity on the Export Control Watch Lists will require a license, even if the shipment is through a reseller or via another country. The outcome of this Legal determination will be binding. The completion of this check will be recorded in the appropriate records and in the appropriate customer record in GitLab’s salesforce.com system.

**Why?**

_In some cases, an End Use Certificate may be required from the End User of a product to confirm legitimate uses of the product being exported. If you are suspicious of the intentions of a purchaser, consult Legal._

**End Use**
GitLab's Products are designed and intended for commercial use. In the event that GitLab knows or had reason to know that a customer seeks to use GitLab products for improper or nefarious purposes, please contact Legal so that the appropriate End Use statements can be obtained.

If you have any questions or concerns about anything herein, please contact Compliance@gitlab.com.

### Maintain Accurate Financial Records / Internal Accounting Controls

Accurate and reliable records are crucial to our business. Records will be maintained accurately to:

- ensure legal and ethical business practices
- prevent fraudulent activities
- ensure that the information we record, process, and analyze is accurate, and recorded in accordance with applicable legal or accounting principles
- ensure that it is made secure and readily available to those with a need to know the information on a timely basis

GitLab records include:

- booking information
- payroll
- timecards
- travel and expense reports
- e-mails
- accounting and financial data
- measurement and performance records
- electronic data files
- all other records maintained in the ordinary course of our business

There is never a reason to make false or misleading entries. Undisclosed or unrecorded funds, payments, or receipts are inconsistent with our business practices and are prohibited.

### Manage Records Properly

Our records are our corporate memory, providing evidence of actions and decisions and containing data and information critical to the continuity of our business.

Records consist of all forms of information created or received by GitLab, whether originals or copies, regardless of media. Examples of company records include:

- paper documents
- e-mail
- electronic files stored on disk
- tape or any other medium (CD, DVD, USB data storage devices, etc.) that contains information about our company or our business activities

We are responsible for properly labeling and carefully handling confidential, sensitive, and proprietary information and securing it when not in use. We do not destroy official company documents or records before the retention time expires, but do destroy documents when they no longer have useful business purpose.

### Avoiding Conflicts of Interest

We have an obligation to make sound business decisions in the best interests of GitLab without the influence of personal interests or gain. Our company requires you to avoid any conflict, or even the appearance of a conflict, between your personal interests and the interests of our company.

A conflict exists when your interests, duties, obligations or activities, or those of a family member are, or appear to be, in conflict or incompatible with the interests of GitLab. Conflicts of interest expose our personal judgment and that of our company to increased scrutiny and criticism and can undermine our credibility and the trust that others place in us.

Should any business or personal conflict of interest arise, or even appear to arise, you should [disclose it immediately to leadership for review](/handbook/people-group/contracts-and-international-expansion/#approval-for-outside-projects--activities-aka-exceptions-to-piaa). In some instances, disclosure may not be sufficient and we may require that the conduct be stopped or that actions taken be reversed where possible. As it is impossible to describe every potential conflict, we rely on you to exercise sound judgment, to seek advice when appropriate, and to adhere to the highest standards of integrity.

**GitLab is often required to disclose any organizational or personal conflict of interest to various third parties. To ensure accuracy in our disclosures, please notify Compliance at Compliance@gitlab.com of any organizational or personal conflicts of interest.**

### Political Activities and Contributions

You may support the political process through personal contributions or by volunteering your personal time to the candidates or organizations of your choice. These activities, however, must not be conducted on company time or involve the use of any company resources. You may not make or commit to political contributions on behalf of GitLab.

### Charitable Contributions

We support community development throughout the world. GitLab employees or contractors may contribute to these efforts, or may choose to contribute to organizations of their own choice. However, as with political activities, you may not use company resources to personally support charitable or other non-profit institutions not specifically sanctioned or supported by our company. You should consult the Legal department if you have questions about permissible use of company resources.

## Code of Business Conduct & Ethics Acknowledgment Form

Team members will review and sign the [Code of Business Conduct & Ethics Acknowledgment Form](https://docs.google.com/document/d/1ehxrg-ieUEx1ARZv4LpysMfxzFmnhgtzmJn1-299g_E/edit?usp=sharing) during onboarding as well as annually during the [Global Compensation Annual Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) cycle. Please access the form template by clicking [here](https://docs.google.com/document/d/1ehxrg-ieUEx1ARZv4LpysMfxzFmnhgtzmJn1-299g_E/edit?usp=sharing).

During the annual merit cycle, people operations will be responsible for sending all team members the updated Code of Business Conduct & Ethics acknowledgment form and ensuring all team members review and acknowledge. The Code of Conduct will be sent to each team member via BambooHR. To review and sign the document: log into BambooHR download the PDF and click on the Code of Conduct Link. Once a team member has reviewed the Code of Conduct they will use the BambooHR signature and date tool to complete the acknowledgement. No changes should be made to the Code of Business Conduct & Ethics without prior approval from the VP of Legal.

Team members that joined prior to the established requirement to upload the acknowledgement at hire are the exception but will be subject to capturing their acknowledgement during annual reviews.
