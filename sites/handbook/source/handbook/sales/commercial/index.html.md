---
layout: handbook-page-toc
title: "Commercial Sales"
department: "The Commercial Sales department at GitLab focuses on delivering maximum value to SMB and Mid-Market customers throughout their entire journey with GitLab"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Commercial Sales Handbook

The Commercial Sales department is part of the overall [GitLab Sales](/handbook/sales) Division. We focus on delivering maximum value to [SMB and Mid-Market](/handbook/business-ops/resources/#segmentation) customers throughout their entire journey with GitLab.

## <i class="fas fa-users fa-fw icon-color font-awesome"></i>Commercial Team Groups

- SMB Account Executives
- Mid Market Account Executives
- Area Sales

## Getting Started as a New Hire

Please ensure you read the [Commercial Sales onboarding page](/handbook/sales/commercial/enablement/).

### Top Tips to Onboard Quicker

1. Bookmark the relevant SalesForce dashboard for your region, this is essential to your success. Your Manager will give you the link when you first join.
1. Get familiar with the [close date conventions](https://docs.google.com/document/d/1TK3X22rBPIOwR9PkopK4snmVVkyCyvdTwKJ_eszH48I/edit?usp=sharing) used in Commercial Sales.
1. Ensure you fully understand [the Required 7](/handbook/sales/commercial/#required-7). Your teammates and manager are more than happy to help you with this if you need it.
1. Read through reviews of GitLab to get an idea of what customers think about the product. Two good places to start are [Softwareadvice.com](https://www.softwareadvice.com/project-management/gitlab-profile/reviews/) and [Gartner Reviews](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab).
1. Watching videos is core to your onboarding. You can watch YouTube & Chorus videos at 1.5x or 1.75x speed to get through them efficiently, you can still retain the information but in less time. Go to the Settings widget > Playback speed.
1. When you first start, searching through Slack is a great way to find useful information as it's our primary method of asking questions and announcing things to the company. Some channels that are particularly good for information:
[#questions](https://gitlab.slack.com/archives/C0AR2KW4B)
[#smb](https://gitlab.slack.com/archives/CH4KPGS87)
[#sales](https://gitlab.slack.com/archives/C02NE5PQM)
[#smb_amer](https://gitlab.slack.com/archives/C010YDVD1BP)
[#international-smb](https://gitlab.slack.com/archives/CN84VB75H)
[#smb_training_support](https://gitlab.slack.com/archives/CKYLWKGJU)
[#competition](https://gitlab.slack.com/archives/C1BBL1V3K)
1. Use [BuiltWith.com](https://builtwith.com/) to get an idea of what technologies a customer is using currently (though be sure to verify this with them)
1. You might find it useful to create a Google doc to document all your learnings in your first 30 days, as you will get a lot of information to remember and it will also get you in the habit of taking notes, a [key part of GitLab’s culture](/handbook/communication/#external-communication).
1. You can never have too many [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with colleagues. You don’t need to ask permission from people, just put some time in their diary.
1. Make sure your calendar is always up to date so that you can get meetings from SDRs without having to moving them around.
1. Listen to as many [Chorus](https://chorus.ai/) calls as possible. We only record calls with the customer's permission in the US, but they are an excellent way to onboard even if you are not based in the US yourself.
1. Add your own tips to this page as you onboard to pay it forward for the next new rep!

### Continuous Learning

Below is a list of resources considered highly valuable for continuous learning and development by Commercial Sales team members at GitLab. Note that some of these resources are not publicly available, so consult a fellow teammate for access where necessary.

- [GitLab Direction](/direction/#vision-1) Includes GitLab’s Vision, [Maturity](/direction/#maturity) framework and planned features and their release dates within each of GitLab’s [Paid Tiers](/direction/#paid-tiers).
- [GitLab Blog](/blog/) Monthly articles on Releases, Engineering, Open Source, Culture and other popular topics of interest.
- [GitLab Unfiltered YouTube](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) A YouTube channel private to GitLab employees with video recordings and playlists on company meetings, enablement sessions and other topics. Access GitLab's [Sales Enablement playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX).
- [Chorus Recordings](/handbook/business-ops/tech-stack/#chorus) Our team's history of call and demo recordings is a valuable training resource for new and existing sales team members.
- [Sales & SDR Enablement Sessions](/handbook/sales/training/sales-enablement-sessions/) Weekly training sessions covering popular product, market and sales training topics chosen by the sales and marketing team members.
- [Product Study Guide](/handbook/marketing/strategic-marketing/tiers/) A study course for team members to understand the current full scope of capabilities offered in each tier of GitLab and reasons for tier upgrades.
- [Sales Training](/handbook/sales/training/) page

### Events

When traveling please consider ROI of both any costs plus your time:
If you are going to an event you think first:

- What is the total you will spend for travel?
- What is the total you will spend on food and lodging for me?
- What is the amount you will likely spend on customer meals (meals x customers x # of times)?
- How many total business hours will you be away from salesforce?
- 8 hours: then you need to get 5x return on $, and the fact that any event pipeline closes at about 30%
- How many total personal life hours will you be away from your personal life?

Given the rough calculation above, you've to be committed driving/booking enough iACV at the event prior to you going to the event
Note: this doesn’t mean you can’t get travel approval without booking meetings. It means once you get travel approval it is on you to go get people to the event/meetings to create pipeline.

#### Trip Notes

The ultimate purpose of attending any GitLab field event is to gain insight and add value. Sid Sijbrandij, our CEO, talks about the importance of conferences and events [here](https://www.youtube.com/watch?v=qQ0CL3J08lI). He believes the main goal of GitLab team participation should always be to connect with our customers and prospects and not to solely attend presentations. Trip notes are our internal process for tracking and capturing this data.

#### Why and When?

Trip notes are required for the entire Commercial Sales Team anytime a team member travels for the company while working at GitLab. We believe it would be fun and valuable if you also shared trip notes when you traveled for personal reasons; however, that is up to your discretion.

#### How to properly document your trip notes?

Please follow the best practice process and use the [trip note template](https://docs.google.com/document/d/1B6EBhrzMhHynDUV6_K2qzuQqZ1jI_DfjCsUviJI_r_k/edit) as a format to prepare for your future trip.

#### Trip Notes Best Practice

1. Commercial attendee will write up their notes in a google doc.
1. Please save your notes as GitLab internal read only for Commercial Team review in the [Commercial Sales Drive Trip Notes Folder](https://drive.google.com/drive/folders/1e71aL5OP_QlaAHTmMNwAAqIUfc2aD6Dt).
1. Once saved, please slack and share a link to your notes in the commercial_global_all channel to alert the team of your recent trip.
1. Finally, please share your [Post Trip feedback](/handbook/marketing/events/#post-event) for the entire company and the Field Marketing team directly in the specific Event Issue on the ‘Post Event Recap & Feedback’ spreadsheet.

### Give Back Project

#### What is the Give Back Project?

- The Give Back Project is a self-selected opportunity to help the Commercial Sales team achieve better results. It is a finite project that, upon completion, is to be adopted by no less than 70% of the Commercial Sales team and used at least once a month.
- The Give Back Project is assigned to all new Commercial Sales team members and is required to be completed as part of their on-boarding process.

#### Why is there a Give Back Project?

- The goal of the Give Back Project is to allow new team members to be able to instantly contribute to the Commercial Sales team
- Fosters GitLab’s values of collaboration and results
- The Give Back Project gives team members the opportunity to be exposed to problems on the team today and will allow the team to look back and see how we addressed shortcomings in the past

#### How does the Give Back Project differ from the Center of Excellence?

- The COE is assigned while the Give Back is self-selected
- During team QBRs in February 2019, it was determined that there were certain aspects that made selling at GitLab difficult. Those aspects were divided into categories and those categories were assigned to different team members to become experts at, forming the Center of Excellence
- The COE has already been determined by the team as areas where the team needs improvements while the value of a team member’s Give Back Project needs to be realized
- A Give Back Project can be working on a portion of the COE

#### How to properly document a Give Back Project

- Give Back Projects are described and worked by team members in the [Commercial Sales issue tracker](https://gitlab.com/gitlab-com/sales-team/commercial-sales/issues?label_name%5B%5D=Give+Back+Project)
- If it is something that needs to be repeated (like a customer AMA) an associated handbook entry is created with a description on how a new person could achieve this
- Although the Give Back Project is designed to be finite, there is always room for iteration at GitLab. Be sure to include any necessary instructions on how a different team member would be able to add to your project if necessary

#### Example Give Back Projects

**Customer AMAs:** Meetings for GitLab team members with current customers that provide an opportunity to ask them about anything in regards to their experience with GitLab

**Customer Meetings:** Sharing the best practices for conducting any kind of external meeting at GitLab

**Trip Notes:** allows team members who attend field events to track and capture data in order for the rest of the team to gain insight

**Doing Business with Partners/Channels:** Guidelines on how to work with partners and resellers in order to ensure value is maximized in these areas

**How to Handle New Customers After They Purchase:** Best practices for handling new GitLab customers based on the scope of their purchase

**Technical Evaluation of Customers:** How to properly evaluate a customer’s current technical environment in order to create organizational alignment and prescribe how GitLab can best solve their problems

For a complete list of our Give Back Projects and other Onboarding assignments our team is working on, please click the private link [here](https://docs.google.com/spreadsheets/d/1_G06Zj8ZWXrfrH75t3BEa8NX3wi6vjQCohFchnPpNUI/edit#gid=0).

## Required 7

New team members should familiarize themselves with [The Required 7](/handbook/sales/commercial/enablement/required7/). These are the 7 tactical skills required to be an excellent Commercial Sales Account Executive:

1. [Ranking your accounts](/handbook/sales/commercial/#account-ranking) - [video here](https://youtu.be/M-5OhlYxmFI)
    - Rank all accounts and explain why; log your sources for future efficiency; this needs to be your (recent) tiering, not the previous owners, nor the tiering from the prior fiscal. If it’s unchanged, make the note that it is unchanged and why
1. Building your [custom deck](https://docs.google.com/presentation/d/1duy8kjEvNiRx8fnCvc6ZnxWwXTM_71phbG3k9rgmU4Q/edit?usp=sharing) for any opp amount over $5k SMB $10k MM **Amount** (n.b. amount, not iACV) that:
    - Calculates the ROI of using GitLab vs. next best option (this might be vs. do nothing)
    - Provides the 2 options for the customer going forward - normally both with GitLab (example: Ult or Prem)
    - Demonstrates a customised vision for the customer: “what if you used all of GitLab to do Devops in a single app?”
    - Capture the full response, in the customers’ words to that vision and the reasons why they think it is possible, or not possible. This capture can be done in the speaker notes so your customer can validate and then copied into an activity after the call.
    - [video here](https://youtu.be/9Yq23PijO3c), and be sure to check out [this video](https://youtu.be/QDoXhDvkDYY) on creating an effective ROI slide deck and [this supporting deck](https://docs.google.com/presentation/d/1otCJF4kaaozhp0qWD3VgGQ73WgX1ZNP_IKnu3Qt_slI/edit?usp=sharing)
1. Capturing key information:
    - How did the customer/prospect hear about GitLab and what is their understanding of what we do?
    - Meaningful information on why we won/lost a deal
1. Logging your [call notes](/handbook/sales/commercial/enablement/#sales-note-taking) [video on taking efficient notes during a meeting](https://youtu.be/CMMQ5R0pq8I) and top tips from a top performer [here](https://www.youtube.com/watch?v=CKRvxM2F5zc)
1. [Updating your next steps](/handbook/sales/commercial/#opportunity-next-steps-best-practices) - [video here](https://youtu.be/74OKSNWoN8Q)
1. [Command plans](/handbook/sales/command-of-the-message/command-plan/), what needs to be filled in:
    - $5k SMB, $10k MM **Amount** - All fields in opportunity overview (the whys) and close-plan over this Amount threshold
    - $10k SMB, $20k MM **iACV** - Full Command Plan over this iACV threshold
    - If you don’t yet know the answers or have the data, you will populate the Command Plan fields with your script and next steps to get that information
    - Exception to command plan: for channel managed opportunities we may not have access - put `Channel-Managed` in the opportunity title
    - ([training session here](https://youtu.be/zN_0J6syxmM) and [training deck here](https://bit.ly/2WNlzbZ))
1. Keeping your opportunities [up to date at all times](/handbook/sales/commercial/#salesforce-hygiene--activity) [video](https://youtu.be/o5gm7_5321w)
## [Opportunity Consults](https://about.gitlab.com/handbook/sales/command-of-the-message/opportunity-consults/) (OCF) and Lightweight Deal Review (LDR) Process

#### SMB
* An Opportunity Consult happens when the opportunity IACV threshold is $10K +
* A Light Weight Deal Review happens when the opportunity IACV threshold is $5K and is forecasted to close this month or next month. 
#### Mid Market
* An Opportunity Consult happens when the opportunity IACV threshold is $20K +
* A Light Weight Deal Review happens when the opportunity IACV threshold is $10K and is forecasted to close this month or next month. 

### Opportunity Next Steps Best Practices

1. Always keep Next Steps Date and Next Steps up to date as an assignment to a future version of yourself on how you will take action to continue the customers buying process.
1. When you start your day you need to check all inputs like email, zendesk, and lead queue so you can triage those requests into your priorities for that day.
1. Allocate times of your day in four ways:

- Customer meetings scheduled
- Proactive email and internal efforts
- Proactive calling efforts
- Time to check inputs again

1. When beginning a proactive time block, open your opportunities view and sort by Next Steps Date.
1. Your Next Steps should be specific and compelling so that you can take action on them within 3 minutes of reading.

### Opportunity-Specific Slide Deck

It is required that all opportunities above $10,000 (Mid Market) and $5,000 (SMB) in total amount (not iACV) will have a custom slide deck that is shared with the customer or prospect. The custom deck must be attached to the opportunity by pasting the deck link in the SalesForce field 'Link to Custom Pitch Deck' on the opportunity home page. This will enable Mid Market Account Executives and SMB Account Executives to capture critical account information. Additionally, this will facilitate selling on value rather than features.

#### Custom Deck Requirements

1. **Visual Requirements**

- Deck's main color will match or resemble that of the customer's.
- Where possible, use the customer's logo.
- It is appropriate to ask the customer for a blank template.

1. **Calculated ROI of GitLab v. Next Best Option**

- For New Business the next best option could be 'do nothing' or our competition.
- For Renewals the next best option could be to go back to what they were doing before.

1. **Two options (rough plans) with GitLab going forward**

- For new business this takes the form of [Two Lane Selling](https://docs.google.com/document/d/1h7DCIRAQ1pRiqDF6Aebl354uoMXiXbSVLaC_XefzM6w/edit) where we give them a high engagement and a low engagement option.
- For a renewal, this could be a one year v. a three year option, or Premium v. Ultimate.

1. **Non-Custom Single Application pitch to the customer**

- This slide should have a visual representation of using GitLab as a single application for the entire software development life cycle.
- The intent of this slide is to have the conversation with the customer on 'what is possible', and to capture their feedback

1. **Full capture of the customer's feedback on the 'Single Application' slide**

- Capture the customer's actual words in the speaker notes of the 'single application' slide

## SMB Account Executives

SMB Account Executives "SMB AEs" act as Account Executives and the face of GitLab for [SMB](/handbook/business-ops/resources/#segmentation) prospects and customers. They are the primary point of contact for companies that employ between 1 to 99 employees. These GitLab team members are responsible for working new business in their [territory](/handbook/sales/territories) as well as handling the customer journey for new and existing customers. SMB Account Executives will assist prospects through their evaluation and buying process and will be the customer's point of contact for any renewal and expansion discussions. [Please read through the SMB handbook page for more information](/handbook/sales/commercial/smb).

#### New Business

- Work Initial Qualification Meetings [IQM's](/handbook/sales/commercial/#inbound-queue-management) from Commercial SDR Team
- Accept Sales Accepted Opportunities according to [SAO Criteria](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)
- Nurture and help prospects during their Trial evaluation after Accepting Opportunities
- Push to Webdirect or help with the Sales Order Process

#### Customer Journey

- [Inbound Queue Management](/handbook/sales/commercial/#inbound-queue-management)
- [Licensing/Subscription Management](/handbook/sales/commercial/#licensingsubscription-management)
- [Troubleshooting Resource for Licensing/Subscription Management](/handbook/sales/commercial/#troubleshooting--how-to-resources-for-licensing--subscription-management)
- [Quotes / Sales Order Processing](/handbook/sales/commercial/#quotes--sales-order-processing)

### Inbound Queue Management

**Zendesk:** Managing incoming requests received through support.gitlab.com, support@gitlab.com and renewals@gitlab.com

**The goal and focus** of working Zendesk tickets for the SMB team is to help with **upselling** and **renewals**

Within timezone (WIP), the Support team is responsible to take the lead on all incoming ZD tickets in the [Upgrades, Renewals, and AR (refunds) queue](https://gitlab.zendesk.com/agent/filters/360034975053). In off-hours, the SMB team will triage and handle any tickets which will breach during their timezone.

Support will take the lead on all incoming requests in the queue and only forward to the sales owner when:

- There is an IACV affecting event from a **customer** -OR-
- There is a credit or other request which requires sales manager approval

### Salesforce Hygiene & Activity

#### Opportunity Management in Salesforce

- Prior to working a new opportunity always double-check the Account segmentation is "SMB" and the employee count for the company is less than 100, by viewing datafox information
- Every SMB AE has an inidividual view for opportunity management: Access this view under the Opportunities tab in saleforce and use the drop down to find your view "SMB_name_ Active Opptys".
- Once in the view follow the below steps for opportunity management;

1. Sort by stage, placing highest priority stage at the top
1. Prioritize the opportunities you are going to work on first by stage, amount, and close date
1. Ensure "Next Steps," "Next Step Date," and "Next Steps Owner" are current with actionable and informative information that helps guide you on how to move the opportunity through the pipeline
1. If a potential future opportunity that require action greater than 30 days out create an event with detailed notes on your agenda or action items
1. Once stages Discovery - Awaiting Signature are updated, begin working on Pending Acceptance. Prioritize with the same criteria as above, rinse and repeat

#### Web Direct Oppty Management in Salesforce

- Find the report view for all CFQ "current fiscal quarter" Web Direct purchases in your territory "SMB_Web Directs CFQ _name":

1. Check that there are no open opportunities that could have been a duplicate by viewing the opportunities listed under the Account
1. If not a duplicate, place the main contact in the opportunity on the outreach sequence "SMB_Web Direct Outreach" - by: Madeline Hennessy
1. Dependent on the opportunity and company if personal outreach is preferred, you must log your activity and have a next step date to follow up on your activity

#### Licensing/Subscription Management

> Note: if you need access to a system, [open an Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request)

License Key Management for On-Premise Trials & Subscriptions

- Access [the LicenseApp](https://license.gitlab.com/) to resend or provide changes to a license key or trial evaluation

GitLab.com Trials & Subscription Management

- Access [the CustomersDot](https://customers.gitlab.com/admin/customer) to provide changes to a user, group, or subscription.

### Troubleshooting & How-To Resources for Licensing & Subscription Management

Detailed instructions on how to handle problems with licenses and subscriptions can be found in [these instructions and resources.](/handbook/business-ops/enterprise-applications/portal/troubleshooting/)

### Quotes | Sales Order Processing

More information about [sales order processing](/handbook/business-ops/order-processing/) can be found in the Business Ops handbook section.

## Mid Market Account Executive

Midmarket Account Executives are the primary point of contact between prospective and existing customers of GitLab within a space defined as mid-market, which currently works with companies that employ between 100 to 1,999 employees. These GitLab team members manage the spectrum of project sizes, ranging from small fast growing teams in smaller agile organizations to complex enterprise projects advising on the journey with GitLab to achieve specific business outcomes.

Mid-Market AEs work closely in tandem with the business development team and sales management to manage a broad book of business spread over a large opportunity value range and focus on exceeding client expectations.

### Core Responsibilities

- Own your territory - build & execute your territory plan.
- Manage book of business: new business, expansion, renewals.
- Collaborate with Sales Development channel for handover of inbound qualified opportunities to progress to closure.
- Execute outbound campaigns for Key accounts.

#### New Business

- Work Initial Qualification Meetings [IQM's](/handbook/sales/commercial/#inbound-queue-management) from SDR Team
- Accept Sales Accepted Opportunities according to [SAO Criteria](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)
- Nurture and help prospects during their Trial evaluation after Accepting Opportunities
- Push to Webdirect or help with the Sales Order Process

#### Selecting Target Accounts for marketing

- Log into Datafox or Sales Navigator
- Input your territory criteria
- Establish a list of accounts and sort by Company Head Count
- Search for key words like DevOps
- Look into each account to see if they are actually yours
- Look into each employee that is either an engineer or technology professionals
- Add that company name to your list
- If not enough companies through this process continue below:
- Look for the intersection of revenue and vertical in Datafox
- Look at the account in SFDC to see if CE User
- Filter using signals if using Datafox
- Assess if they have had recent funding announcements
- Once selected add the company name, city, and zip code to your sheet

#### Customer Journey

- [Inbound Queue Management](/handbook/sales/commercial/#inbound-queue-management)
- [Licensing/Subscription Management](/handbook/sales/commercial/#licensingsubscription-management)
- [Troubleshooting Resource for Licensing/Subscription Management](/handbook/business-ops/enterprise-applications/portal/troubleshooting/)
- [Quotes / Sales Order Processing](/handbook/sales/commercial/#quotes--sales-order-processing)

## Account Ownership Rules of Engagement for Commercial Sales

It's expected from the Commercial Sales Team to follow the [Account Ownership Rules of Engagement](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
This section provides step by step clarity on how to follow the **ROE**.

### Process to follow:

1. Check **Datafox** data - wholly wrong? Go to step 2
1. Check **Zoominfo** data - wholly wrong? Go to step 3
1. Check **LinkedIn** Data - wholly wrong?
1. Search for **3rd party resources** which provide SMB/MM segment prove (only needed when 1 of the above resources do not provide leading data to go with)
1. Paste the source and the gaps all found that led you to it in Chatter on the `Account level` and **cc VP Commercial Sales** to review
1. VP of Commercial Sales will provide the leading decision

**Note:**

- **Priority order:** only go to the next source IF the data number is wholly wrong / inaccurate
- **REQUIRED:** `Never` ask the customer/prospect to provide information on the company size
- **Currently:** Leading decision will be based on the fact if the Account was SMB/MM segment during the **Feb 2019 size clean up**
- **From Q4 on** - Leading decision will be based on the fact if Account was SMB/MM segment based on the **Nov 2019 size clean up**

## Account Ranking

It is a requirement for the Commercial Sales Team to tier their accounts using the fields on the Account Object. This helps prioritize the accounts to go after when prospecting for new or expansion. Please see below definitions specific to SMB and Mid-Market AEs.

### Ranking Training and Definitions

[Commercial Sales: Account Ranking Training Clippet](https://youtu.be/M-5OhlYxmFI)

#### Small Business Account Executive Definitions for Account Ranking

- Tier 1: Top 10 Accounts with new business or upselling possibility within the next 12 months
- Tier 1.5: Future opportunities that we should not lose track of as there is a sales opportunity here with no timeline
- Tier 2: Believe there is an opportunity, but will take additional work before opening and beginning a sales cycle
- Tier 3: Greenfield, SDR attention - perhaps a drip campaign
- SMB AE must have potential user field filled in on the account object for Tier 1, and 1.5
- SMB AE must have the notes section filled in on why an account is Tier 2 or 3

#### Mid-Market Account Executive Definitions for Account Ranking

- Tier 1: Accounts that I am working to maximize their iACV this fiscal year.
- Tier 1.5 The subset of Tier 2 that is best and trending toward being future Tier 1s.
- Tier 2 Accounts that I am working to grow and I speak with them at least once a month.
- Tier 3 Accounts that I keep informed about GitLab and DevOps monthly.

## Center of Excellence

The Center of Excellence (COE) in the Commercial Sales context refers to the team's research and development of a particular focus area to drive new initiatives and improvements across the organization.

Why call it Center of Excellence?

- We have challenges we are uniquely positioned to focus on
- We aren’t the Directly Responsible Individual (DRI) but it is a challenge to get this prioritized
- This is our current platform for collaboration

How?

1. Choose one of the active Topics currently available below. Refer to the team list to see what is unassigned. Note, to take a topic you do not have to be a Subject Matter Expert (SME) in the area or DRI as they may belong to a different Department.
1. Pick an achievable set of commitments and nail it. These should be forecastable and not something too aspirational for the timeframe
1. Decide for yourself if you want that same topic for 3 or 6 months
1. Start with the problem related to your subject that you think needs to be solved

#### Commercial COE Topics

Here are the past active topics for individual team members on COE alignment. This center in its first iteration started with the team's QBR in 2019.

- **Communication and Collaboration**
Support on communication and tools to stay better connected as a remote Commercial Sales organization
- **CustomersDot**
Improvements to our [CustomersDot](https://customers.gitlab.com)
- **Customer Advisory Board**
Capturing direct feedback for interaction and organizing customers selected
- **Feedback Loop**
Improvements to feedback channels to document structured data for all customer interactions
- **Hosted gitlab.com**
Ramping new customers and users on gitlab.com, our hosted product offering
- **KISS enablement**
Keep it simple. Focus on process including managing tasks, training GitLab, and new release features
- **Marketing campaigns**
- **Sales content/collateral**
- **Sales process - SDR**
Improvements to the SDR to AE collaboration process
- **Services**
Improvements to the Professional Services offering
- **Ultimate**
Improvements to the full pitch including [Why Ultimate](/pricing/ultimate/)

## Other Related Pages

- [Commercial Sales - Customer Success](/handbook/customer-success/comm-sales)
- [Territories](/handbook/sales/territories)
- [Sales Hiring Chart](/handbook/hiring/charts/sales)
