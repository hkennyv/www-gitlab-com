---
layout: handbook-page-toc
title: "Data Driven Recruiting"
description: "Data Driven Recruiting is defined as focusing on fewer people that have more aptitude and connection to the company."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Data Driven Recruiting

[Dave Gilbert](https://about.gitlab.com/company/team/#davegilbert), [Sid Sijbrandij](https://about.gitlab.com/company/team/#sytses) and Brendan Browne discuss how to generate a Data Driven Recruiting Model.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8qNxeEJimpU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Data Driven Recruiting at GitLab

Data Driven Recruiting is defined as focusing on fewer people that have more aptitude and connection to the company. 

TODO: VP of Recruiting to add iterative steps to works towards following a Data Drive Recruiting model at GitLab.
