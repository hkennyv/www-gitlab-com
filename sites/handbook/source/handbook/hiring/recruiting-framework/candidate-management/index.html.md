---
layout: handbook-page-toc
title: "Candidate Management Processes"
description: "Provide guidance on managing candidates in Greenhouse."
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Candidate Management Processes
**Purpose**: To provide guidance on managing candidates in Greenhouse.

### Greenhouse Sourcing
<details>
  <summary markdown='span'>
How to Transfer a Candidate That Was Rejected to a Different Requisition
  </summary>
If you are sourcing through Greenhouse and want to consider a rejected candidate for a different requisition:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, select the `Pencil Icon` to change the source to `Greenhouse Sourcing` and include the name of the person responsible for sourcing.
</details>

<details>
  <summary markdown='span'>
How to Transfer a Candidate to a Different Requisition
  </summary>
If you want to consider a candidate for a different requisition:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, be sure to select the `Pencil Icon` to update the source if necessary.
</details>

<details>
  <summary markdown='span'>
How to Transfer a Rejected Candidate to the Same Requisition
  </summary>
If you want to consider a candidate for the same requisition they were rejected from:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, be sure to select the `Pencil Icon` to update the source if necessary.
</details>
